/** Created by andrey on 30.06.16. **/


define(['./module'], function (filters) {
    'use strict';
    filters.filter('pictureFileExtensions', function(){
        return function(str) {

            var variableList = str.split('.');
            var newClass = variableList.pop();

            if ( newClass in {'doc':0, 'docx':1, 'odt':2, 'txt':3, 'dot':4} ) {
                return 'classWord';
            } if ( newClass in {'pptx':0, 'ppsx':1} ) {
                return 'classPptx';
            } if ( newClass in {'xls':0, 'xlt':1, 'ods':2, 'uos':3, 'xlsx':4, 'csv':5}) {
                return 'classXls';
            } else {
                return 'classGeneral';
            }
        };
    });
});