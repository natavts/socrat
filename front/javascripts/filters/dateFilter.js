/**
 * Created by emmtech on 24.06.16.
 */
define(['./module'], function (filters) {
    'use strict';
    filters.filter('dateFilter', function(){
        return function(str) {
            var temp = (str.split('T'))[0].split('-');
            return temp[2]+'.'+temp[1]+'.'+temp[0]
        }
    });
});