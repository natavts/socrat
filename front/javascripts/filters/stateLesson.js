/**
 * Created by emmtech on 17.06.16.
 */
define(['./module'], function (filters) {
    'use strict';
    filters.filter('stateLesson', function(){
        return function(str) {
            switch (str) {
                case null:
                    return 'не начат';
                    break;
                case 1: 
                    return 'начат';
                    break;
                case  2:
                    return 'приостановлен';
                    break;
                case 3: 
                    return 'завершен';
                    break;
            }
        }
    });
});