define(['./module'], function (directives) {
  'use strict';
  directives.directive('dropDown', function () {
    return {
      restrict: 'E',
      replace: true,
      controller: ['$scope', function ($scope) {

        // https://docs.angularjs.org/api/ng/function/angular.isObject
        // отслеживать изменения $scope.options
        // когда в него придет object => создать $scope.objectOptions;
        // TODO: лютый костыль, переписать
        $scope.$watch("list", function (newValue) {
          if (angular.isObject(newValue) && !angular.isArray(newValue)) {
            $scope.objectOptions = newValue;
            $scope.showKeyByValue = function () {
              if (isNaN(+$scope.mod) && typeof $scope.mod === "string") {
                return $scope.mod;
              } else {
                return $scope.objectOptions[$scope.mod];
              }
            }
          }
        });
        $scope.editMod = function (val) {
          $scope.showDropDown1 = false;
          $scope.mod = val;
          $scope.$emit('dropDownNewValue');
        }
      }],
      scope: {
        list: "=",
        mod: "=model"
      },
      templateUrl: '/templates/directives/dropDown.html'
    }
  });
});
