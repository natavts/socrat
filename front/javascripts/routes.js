define(['./app'], function (app) {
    'use strict';
    return app.config(function($stateProvider, $urlRouterProvider, $locationProvider ) {
        // Любые неопределенные url перенаправлять на /err404
        $urlRouterProvider.otherwise("/err404");
        $stateProvider
            //Пользователи
            .state('home', {
                url: "/",
                templateUrl: "/templates/home.html",
                controller: 'Home',
                data: {
                    title: 'Вход'
                }
            })

            //Записи
            .state('records', {
                url: "/records?offset&sort&filter",
                templateUrl: "/templates/records/list.html",
                controller: 'Records',
                data: {
                    title: 'Записи',
                    headerButtons: [{title:'Расписание', state:'schedule'}]
                }
            })
            //Записи - Просмотр записи - пример
            .state('records.view', {
                url: "/view-record?id",
                templateUrl: "/templates/records/view-record.html",
                controller: 'RecordsView',
                data: {
                    title: 'Просмотр записи',
                    breadcrumbs : [{'title':'Записи','state':'records'}],
                    headerButtons: [{title:'Расписание', state:'schedule'}]
                }
            })
            //Записи - Во весь экран
            .state('records.full-screen', {
                url: "/full-screen",
                templateUrl: "/templates/records/full-screen.html",
                data: {
                    title: 'Во весь экран',
                    breadcrumbs : [{'title':'Записи','state':'records'}]
                }
            })
            //Записи - Редактировать запись
            .state('records.edit-view', {
                url: "/edit-view?id",
                params: {
                    editRecordSucsessful: null
                },
                templateUrl: "/templates/records/edit-view.html",
                controller: 'RecordSaved',
                data: {
                    title: 'Редактировать запись',
                    breadcrumbs : [{'title':'Записи','state':'records'}]
                }
            })
            //Пользователи
            .state('users', {
                url: "/users?offset&sort&filter",
                params: {
                    newUser: null
                },
                templateUrl: "/templates/users/list.html",
                controller: 'Users',
                data: {
                    title: 'Пользователи'
                }
            })
            //Импорт пользователей
            .state('users.import', {
                url: "/import",
                templateUrl: "/templates/users/import.html",
                controller: 'Users',
                data: {
                    title: 'Импорт пользователей',
                    breadcrumbs : [{'title':'Пользователи','state':'users'}]
                }
            })
            //Создать пользователя
            .state('users.create', {
                url: "/create",
                params: {
                    newUser: null
                },
                templateUrl: "/templates/users/create.html",
                controller: 'UserCreate',
                data: {
                    title: 'Создать пользователя',
                    breadcrumbs : [{'title':'Пользователи','state':'users'}]
                }
            })
            //Показать пользователя
            .state('users.userview', {
                url: "/userview?id",
                templateUrl: "/templates/users/userview.html",
                controller: 'UsersView',
                data: {
                    title: 'Данные пользователя',
                    breadcrumbs : [{'title':'Пользователи','state':'users'}]
                }
            })
            //Редактирование пользователя
            .state('users.edit', {
                url: "/edit?id",
                templateUrl: "/templates/users/edit.html",
                controller: 'UserCreate',
                data: {
                    title: 'Редактирование пользователя',
                    breadcrumbs : [{'title':'Пользователи','state':'users'}]
                }
            })
            .state('superadmin-users',{
                url: "/superadmin-users?offset&sort&filter",
                templateUrl: "/templates/users/superadmin/list.html",
                controller: 'Users',
                data: {
                    title: 'Пользователи'

                }
            })
            .state('superadmin-users.view',{
                url: "/view?id",
                params: {
                    superAdminEdit: null
                },
                templateUrl: '/templates/users/superadmin/view.html',
                controller: 'UsersView',
                data:{
                    title: 'Данные пользователя',
                    breadcrumbs: [{'title':'Пользователи', 'state':'superadmin-users'}]
                }
            })
            .state('superadmin-users.edit', {
                url: '/edit?id',
                templateUrl: '/templates/users/superadmin/edit.html',
                controller:'UserCreate',
                data: {
                    title: 'Редактирование пользователя',
                    breadcrumbs : [{'title':'Пользователи','state':'superadmin-users'}]
                }
            })
            //Кабинеты
            .state('offices', {
                url: "/offices?offset&sort&filter",
                templateUrl: "/templates/offices/list.html",
                controller: 'Office',
                data: {
                    title: 'Кабинеты'
                }
            })
            //Данные кабинета
            .state('offices.data', {
                url: "/data?id",
                templateUrl: "/templates/offices/data.html",
                controller: 'OfficesEdit',
                params: {
                  save: false
                },
                data: {
                    title: 'Данные кабинета',
                    breadcrumbs : [{'title':'Кабинеты','state':'offices'}]
                }
            })
            //Редактировать кабинет
            .state('offices.edit', {
                url: "/edit?id",
                templateUrl: "/templates/offices/edit.html",
                controller: 'OfficesEdit',
                data: {
                    title: 'Редактировать кабинет',
                    breadcrumbs : [{'title':'Кабинеты','state':'offices'}]
                }
            })
            //Создать кабинет
            .state('offices.create', {
                url: "/create",
                templateUrl: "/templates/offices/create.html",
                controller: 'OfficeCreate',
                data: {
                    title: 'Создать кабинет',
                    breadcrumbs : [{'title':'Кабинеты','state':'offices'}]
                }
            })
            //Классы
            .state('classes', {
                url: "/classes?offset&sort&filter",
                templateUrl: "/templates/classes/list.html",
                controller: 'Classes',
                data: {
                    title: 'Классы'
                }
            })
            //Создать класс
            .state('classes.create', {
                url: "/create",
                templateUrl: "/templates/classes/create.html",
                controller: 'ClassesCreate',
                data: {
                    title: 'Создать класс',
                    breadcrumbs : [{'title':'Классы','state':'classes'}]
                }
            })
            //Показать класс
            .state('classes.classview', {
                url: "/classview?id&newClass",
                templateUrl: "/templates/classes/classview.html",
                controller: 'ClassesView',
                data: {
                    title: 'Данные класса',
                    breadcrumbs : [{'title':'Классы','state':'classes'}]
                }
            })
            //Редактировать класс
            .state('classes.edit', {
                url: "/edit?id",
                templateUrl: "/templates/classes/edit.html",
                controller: 'ClassesEdit',
                data: {
                    title: 'Редактировать класс',
                    breadcrumbs : [{'title':'Классы','state':'classes'}]
                }
            })
            //Профиль
            .state('profile', {
                url: "/profile",
                templateUrl: "/templates/profiles/list.html",
                data: {
                    title: 'Профиль'
                }
            })

            //Редактировать профиль
            .state('profile.edit', {
                url: "/edit",
                templateUrl: "/templates/profiles/edit.html",
                data: {
                    title: 'Редактирование профиля',
                    breadcrumbs : [{'title':'Профиль','state':'profile'}]
                }
            })
            //Сохранить профиль
            .state('profile.save', {
                url: "/save",
                params: {
                    save: true
                },
                templateUrl: "/templates/profiles/list.html",
                controller: 'ProfilesSave',
                data: {
                    title: 'Редактирование профиля',
                    breadcrumbs : [{'title':'Профиль','state':'profile'}]
                }
            })
            //Предметы
            .state('lessons', {
                url: "/lessons?offset&sort&filter",
                params: {
                    created: false
                },
                controller: 'Lessons',
                templateUrl: "/templates/lessons/list.html",
                data: {
                    title: 'Предметы'
                }
            })

            //Просмотр предмета
            .state('lessons.view', {
                url: "/view?id&newLesson",
                controller: 'LessonView',
                templateUrl: "/templates/lessons/view.html",
                data: {
                    title: 'Просмотр предмета',
                    breadcrumbs : [{'title':'Предметы','state':'lessons'}]
                }
            })
            //Редактировать предмет
            .state('lessons.edit', {
                url: "/edit?id",
                controller: 'LessonEdit',
                templateUrl: "/templates/lessons/edit.html",
                data: {
                    title: 'Редактировать предмет',
                    breadcrumbs : [{'title':'Предметы','state':'lessons'}]
                }
            })
            //Создать предмет
            .state('lessons.create', {
                url: "/create",
                controller: 'LessonsCreate',
                templateUrl: "/templates/lessons/create.html",
                data: {
                    title: 'Создать предмет',
                    breadcrumbs : [{'title':'Предметы','state':'lessons'}]
                }
            })
            //Сохранить предмет
            .state('lessons.created', {
                url: "/created",
                params: {
                    created: true
                },
                templateUrl: "/templates/lessons/list.html",
                controller: 'LessonsCreated',
                data: {
                    title: 'Предметы',
                    breadcrumbs : [{'title':'Предметы','state':'lessons'}]
                }
            })

            //Создать предмет
            .state('subjects', {
                url: "/subjects?offset&sort&filter",
                templateUrl: "/templates/subjects/list.html",
                controller: 'Subject',
                data: {
                    title: 'Уроки'
                }
            })
            //Логин Учителя
            .state('teacher', {
                url: "/teacher",
                templateUrl: "/templates/teacher/home.html",
                data: {
                    title: 'Вход'
                }
            })
            //Расписание
            .state('schedule', {
                url: "/schedule?offset&sort&filter",
                controller: 'Schedule',
                params: {
                    showNoteUpdated: false
                },
                templateUrl: "/templates/schedule/list.html",
                data: {
                    title: 'Расписание',
                    headerButtons: [
                        {title:'Архив уроков', state:'records'},
                        /*{title:'Расписание', state:'schedule'}*/
                    ]
                }
            })

            // .state('test', {
            //     url: "/test",
            //     controller: 'Test',
            //     params: {
            //         showNoteUpdated: false
            //     },
            //     templateUrl: "/templates/test.html",
            //     data: {
            //         title: 'Расписание',
            //         headerButtons: [
            //             {title:'Архив уроков', state:'records'},
            //             /*{title:'Расписание', state:'schedule'}*/
            //         ]
            //     }
            // })

            //Редактировать урок УЧИТЕЛЬ
            .state('schedule.edit', {
                url: "/edit?id",
                templateUrl: "/templates/schedule/edit.html",
                controller: 'ScheduleEdit',
                data: {
                    title: 'Редактировать урок',
                    headerButtons: [{title:'Архив уроков', state:'records'}]
                }
            })


            // Добавить урок
            .state('schedule.add', {
              url: '/add',
                params: {
                    showNoteUpdated: false
                },
              templateUrl: '/templates/schedule/add.html',
                controller: 'createSchedule',
              data: {
                  title: 'Добавить урок'
              }
            })
            // Урок добавлен
            .state('schedule.added', {
                url: "/added",
                controller: 'ScheduleAdded',
                params: {
                    showNoteAdded: true
                },
                templateUrl: "/templates/schedule/list.html",
                data: {
                    title: 'Расписание'
                }
            })
            // Начало урока
            .state('lesson', {
                url: '/lesson?id',
                templateUrl: '/templates/lesson/lesson.html',
                controller: 'Lesson',
                data: {
                    title: 'Урок',
                    breadcrumbs: [{'title': 'Расписание', state:'schedule'}],
                    headerButtons: [
                        {title:'Архив уроков', state:'records'},
                        {title:'Расписание', state:'schedule'}
                    ]
                }
            })
            // Начать урок
            .state('lesson.create', {
                url: '/create',
                templateUrl: '/templates/lesson/lesson-create.html',
                controller: 'LessonCreate',
                data: {
                    title: 'Урок',
                    breadcrumbs: [{'title': 'Расписание', state:'schedule'}],
                    headerButtons: [
                        {title:'Архив уроков', state:'records'},
                        {title:'Расписание', state:'schedule'}
                    ]
                }
            })

            // Ученик - урок
            .state('lesson.learner', {
                url: '/learner',
                templateUrl: '/templates/lesson/learner.html',
                controller: 'LessonLearner',
                data: {
                    title: 'Урок',
                    breadcrumbs: [{'title': 'Расписание', state:'lesson.lessonschedule'}]
                }
            })

            // Ученик - Расписание
            .state('lesson.lessonschedule', {
                url: "/lessonschedule?offset&sort&filter",
                templateUrl: "/templates/lesson/lessonschedule.html",
                controller: 'LessonsSchedule',
                data: {
                    title: 'Расписание',
                    breadcrumbs:[]
                }
            })

            // Ученик - Архив
            .state('lesson.lessonrecords', {
                url: "/lessonrecords?offset&sort&filter",
                templateUrl: "/templates/lesson/lessonrecords.html",
                controller: 'LessonsSchedule',
                data: {
                    title: 'Архив',
                    breadcrumbs:[]
                }
            })

            .state('err_404', {
                url: "/err404",
                templateUrl: "/templates/err404.html"
            });
        //Включаем красивые url(требуется html5)
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });
});
