define (['./module'],function (controllers) {
    'use strict';
    controllers.controller('SuperadminView', ['$scope', '$stateParams', function ($scope, $stateParams){
        $scope.superAdminEdit = $stateParams.superAdminEdit?1:0;
        
    }])
});