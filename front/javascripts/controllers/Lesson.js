/**
 * Created by rabota on 06.06.2016.
 */
define(['./module', 'jquery', 'FP'], function (controllers, $) {
    'use strict';
    controllers.controller('Lesson', ['$scope', 'ngSocket', '$state', '$stateParams', '$rootScope', 'FileUploader', '$interval', function ($scope, ngSocket, $state, $stateParams, $rootScope, FileUploader, $interval) {

      $scope.activeButton = 0;


        // изменение громкости
        $scope.camerasList = [];
        $scope.changeVolume07 = function (e) {
            e.stopPropagation();
            var volume07 = $('#volume07').val();
            // var video = $('#mainScreen video')[0];
            // video.volume = volume07/100;
        };

        // добавление файлов
        var uploader = $scope.uploader = new FileUploader({
            url: '/api/upload/uploadFile?scheduleId=' + $stateParams.id
        });

        // FILTERS
        uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
            $scope.getFileList();
        };

        console.info('uploader', uploader);

        $scope.getFileList = function () {
            ngSocket.emit('getFileList', {scheduleId: $stateParams.id});
        };
        if ($scope.$state.current.name === 'lesson' || $scope.$state.current.name.indexOf('lesson.learner') == 0) {
            $scope.getFileList();
        }

        ngSocket.on('fileList', function (result) {
            $scope.filelist = result.files;
            if (result.err) {
                alert(result.message);
                return false;
            }
        });

        // Календарь начало
        Date.prototype.daysInMonth = function () {
            return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
        };
        $scope.buildCalendar = function () {
            var mas = [];
            var z = 0;
            var now;
            if (arguments.length) {
                $scope.currentDate.setMonth(arguments[0]);
                now = $scope.currentDate;
            } else {
                now = new Date();
            }
            now.setDate(1);
            var today = now.getDay();
            var correctToday;
            if (today == 0) {
                correctToday = 7;
            } else {
                correctToday = today;
            }
            mas[0] = [];
            for (var bb = 0; bb < 7; bb++) {
                mas[0][bb] = {name: (bb < correctToday - 1 ? '' : ++z)};
            }
            for (var aa = 1; aa < ((now.daysInMonth() + correctToday - 1) / 7); aa++) {
                mas[aa] = [];
                for (var bb = 0; bb < 7; bb++) {
                    mas[aa][bb] = {name: (z < now.daysInMonth() ? ++z : '')};
                    if (z === (new Date()).getDate()
                        && now.getMonth() === (new Date()).getMonth()
                        && now.getFullYear() === (new Date()).getFullYear()) {
                        mas[aa][bb].current = true;
                    }
                }
            }
            $scope.testVar = mas;
            var month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
            $scope.month = month[now.getMonth()];
            $scope.currentDate = now;
        };
        $scope.previousMonth = function () {
            $scope.buildCalendar($scope.currentDate.getMonth() - 1);

        };
        $scope.nextMonth = function () {
            $scope.buildCalendar($scope.currentDate.getMonth() + 1);
        };
        // Календарь конец
        // Вывод числа по клику на число в календаре начало
        $scope.transferDate = function (name) { // сохраненяем дату в $scope
            var datestring = {};
            $scope.currentDate.setDate(name);
            datestring.date = $scope.currentDate.getDate() < 10 ? '0' + $scope.currentDate.getDate() : $scope.currentDate.getDate();
            datestring.month = $scope.currentDate.getMonth() < 9 ? '0' + ($scope.currentDate.getMonth() + 1) : $scope.currentDate.getMonth() + 1;
            datestring.year = $scope.currentDate.getFullYear() < 10 ? '0' + $scope.currentDate.getFullYear() : $scope.currentDate.getFullYear();
            $scope.transferYMD = datestring.date + '.' + datestring.month + '.' + datestring.year;
        };
        // Вывод числа по клику на число в календаре конец

        ngSocket.on('catchSchedule', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.schedule = result.data;
            if ($scope.$state.current.name === 'lesson') {
                if (result.data.state == 1) {
                    $scope.lessonRun();
                } else if (result.data.state == 2) {
                    $scope.lessonPause();
                } else if (result.data.state == 3) {
                    $scope.lessonEnd()
                }

                // получение списка камер подключенных к этому уроку в этом кабинете
                result.data.officeId > 0
                && ngSocket.emit('getCamerasList', {id: (+result.data.officeId)});
                $scope.$state.current.name === 'lesson'
                && ngSocket.emit('getLearnersList', {classId: result.data.classId})
            }
        });
        $scope.sortLimit = 0;
        $scope.sortParams = null;
        $scope.act = 0;
        $scope.messageSelect = "0"
        $scope.active = function (key, keyes) {
            $scope.action = key;
            $scope.act = keyes;
            console.log(321);
            if ($scope.act == 0) {
                $scope.sortLimit = 20;
                ngSocket.emit('getChatList', {limit: $scope.sortLimit, scheduleId: $stateParams.id});
            } else if ($scope.act == 1) {
                $scope.sortLimit = 10;
                ngSocket.emit('getChatList', {limit: $scope.sortLimit, scheduleId: $stateParams.id});
            } else if ($scope.act == 2) {
                $scope.sortLimit = null;
                ngSocket.emit('getChatList', {limit: $scope.sortLimit, scheduleId: $stateParams.id});
            }
            if (key == 0) {
                $scope.sortParams = {scheduleId: +$stateParams.id, departed: false};
            } else if (key == 1) {
                $scope.sortParams = {armUp: true};
            } else if (key == 2) {
                $scope.sortParams = {departed: true, scheduleId: +$stateParams.id};
            }
        };

        if ($scope.$state.current.name === 'lesson') {
            $scope.active(0, $scope.act);
        }

        $scope.notStart = false;
        ngSocket.on('notStart', function () {
            $scope.notStart = true;
        });

        ngSocket.on('learnerList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.learners = result.data;
        });
        $scope._teacher = {
            id: null,
            setId: function (id) {
                this.id = id;
            }
        };
        var vwLoading = false;
        $scope.initPlayer = function () {
            vwLoading = false;
            var videosList = [];
            if ($scope.$state.current.name === 'lesson' || $scope.$state.current.name === 'lesson.learner') {
                if ($scope.webCam !== null && $scope.camerasList[0].url !== $scope.webCam) {
                    $scope.camerasList.unshift({url: $scope.webCam});
                }
                var f = $scope.f = Flashphoner.getInstance();


                $scope.f.addListener(WCSEvent.ConnectionStatusEvent, function () {
                    //После инициализации
                    //перебрать массив камер

                    $scope.camerasList.forEach(function (nextCamera, i) {
                        //если первая - это веб-камера
                        if (!i && $scope.webCam !== null) {

                            //если я учитель
                            if ($scope.$state.current.name === 'lesson') {
                                //опубликовать поток с вебки учителя
                                f.publishStream({name: 'teacherCam' + $scope.webCam, record: false});
                                //если я ученик
                            } else {
                                //воспроизвести поток с вебки учителя
                                f.playStream({
                                    name: 'teacherCam' + nextCamera.url,
                                    remoteMediaElementId: 'remoteVideo' + (i ? i : '')
                                });
                            }
                            // воспроизвести остальные камеры
                        } else{
                            f.playStream({name: nextCamera.url, remoteMediaElementId: 'remoteVideo' + (i ? i : '')});
                        }
                            // f.playStream();
                    });
                });


                $scope.f.addListener(WCSEvent.StreamStatusEvent, function (event) {
                    switch (event.status) {
                        //если поток опубликовался
                        case StreamStatus.Publishing:
                            //если я ученик отправить учителю ссылку на поток
                            if ($scope.$state.current.name === 'lesson.learner') {
                                ngSocket.emit('callTeacher', {teacherId: $scope._teacher.id, name: event.name});
                            }
                            //если я учитель отправить ученику ссылку на поток
                            if ($scope.$state.current.name === 'lesson' && ('teacherCam' + $scope.webCam) == event.name) {
                                ngSocket.emit('restartTeacherSteam', {
                                    name: $scope.webCam,
                                    sheduleId: +$stateParams.id
                                });
                            }
                            break;
                        case StreamStatus.Unpublished:
                            break;
                        //Если возникли ошибки
                        case StreamStatus.Failed:
                            if (//перебор списка камер возвращающий true, если на нейдена камера, вызвавшая ошибку
                            $scope.camerasList.every(function (nextCamera, i) {
                                //если это тот поток, где ошибка
                                if (event.name === 'teacherCam' + nextCamera.url) {
                                    //если это вебкамера учителя
                                    if ($scope.webCam === nextCamera.url) {
                                        //если я учитель  повторно опубликовать поток
                                        if ($scope.$state.current.name === 'lesson') {
                                            $scope.f.publishStream({name: 'teacherCam' + $scope.webCam, record: false});
                                            //если я ученик повторно воспроизвести поток
                                        } else {
                                            $scope.f.playStream({
                                                name: 'teacherCam' + nextCamera.url,
                                                remoteMediaElementId: 'remoteVideo' + (i ? i : '')
                                            });
                                        }

                                    } else
                                    // если это IP-камера перезапустить поток
                                        $scope.f.playStream({
                                            name: nextCamera.url,
                                            remoteMediaElementId: 'remoteVideo' + (i ? i : '')
                                        });
                                    return false
                                }
                                return true
                            })
                            //если это камера ученика
                            && (event.name.indexOf('camUser') == 0)
                            ) {
                                //если я учитель запустить видео с камеры ученика
                                if ($scope.$state.current.name === 'lesson') {
                                    $scope.f.playStream({name: event.name, remoteMediaElementId: 'learnerVideo'});
                                }
                                //если я ученик начать трансляцию видео с камеры ученика
                                if ($scope.$state.current.name === 'lesson.learner') {
                                    var name = 'camUser' + $rootScope.user.id + Date.now();
                                    $scope.f.publishStream({name: name, record: false});
                                }
                            }
                            break;
                    }
                });
                var configuration = new Configuration();
                configuration.remoteMediaElementId = 'remoteVideo';
                configuration.localMediaElementId = 'localVideo';
                if ($scope.$state.current.name === 'lesson') {
                    configuration.localMediaElementId = 'remoteVideo';
                }
                configuration.elementIdForSWF = "flashVideoDiv";
                var proto;
                var url;
                var port;
                if (window.location.protocol == "http:") {
                    proto = "ws://80.87.201.25";
                    port = "8282";
                } else {
                    proto = "wss://pdosokrat.ru";
                    port = "8443";
                }

                url = proto + ":" + port;
                $scope.f.init(configuration);
                // $scope.f.getAccessToAudioAndVideo();
                $scope.f.connect({urlServer: url, appKey: 'defaultApp'});
            }
        };

        ngSocket.on('camerasList', function (result) {
            if (result.err) {
                alert(result.message);
                return false
            }
            $scope.camerasList = JSON.parse(JSON.stringify(result.data.rows));
            $scope.webCam = result.webCam;
            if (($scope.camerasList && $scope.camerasList.length || $scope.webCam) && vwLoading) {
                $scope.initPlayer();
            }
        });


        ngSocket.emit('getOfficeList', {});
        ngSocket.on('officeList', function (result) {
            $scope.officeList = result.data.rows;
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.offices = {};
            result.data.rows.forEach(function (elem) {
                $scope.offices[elem.id] = elem.number;

            });
        });
        ngSocket.on('scheduleUpdate', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.busy = false;
            setTimeout(function () {
                $scope.timeout = false;
                $scope.$apply();
            }, 2000);
            if (data.data.state == 1) {
                $scope.classRun = true;
                $scope.classPause = true;
                $scope.classEnd = false;
                $scope.editTag = false;
            } else if (data.data.state == 2) {
                $scope.classPause = false;
                $scope.classContinue = true;
                $scope.classRun = true;
                $scope.editTag = false;
            } else if (data.data.state == 3) {
                $scope.classEnd = true;
                $scope.editTag = true;
            }
        });


        // обработка работы чата
        $scope.chat = {};
        $scope.chat.message = '';
        $scope.chat.messages = [];

        ngSocket.on('catchMessageRow', function (result) {
            if (!result.err) {
                // $scope.chat.messages.push({time: new Date(result.time), text:result.message, username:result.userId});
               // if ($scope.chat.messages[$scope.chat.messages.length - 1].createdAt !== result.message.createdAt) {
                    $scope.chat.messages.push({
                        createdAt: result.message.createdAt,
                        message: result.message.message,
                        user: result.user
                    });
            }
        });

        ngSocket.emit('getChatList', {
            limit: $scope.sortLimit == 0 ? 20 : $scope.sortLimit,
            scheduleId: $stateParams.id
        });

        ngSocket.on('chatList', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.chat.messages = JSON.parse(JSON.stringify(data.data));
        });

        $scope.chat.addMessage = function () {
            if ($scope.chat.message) {
                ngSocket.emit('chat', {message: $scope.chat.message, scheduleId: $stateParams.id});
            }
            $scope.chat.message = '';
        };

        $scope.chat.keyUp = function (e) {
            if (e.keyCode === 13) {
                if ($scope.chat.message) {
                    $scope.chat.addMessage();
                    return null;
                }
                $scope.chat.message = '';
            }
        };

        $scope.soundOff = function () { // звук вкл/выкл
            var player = $('#mainScreen video')[0];
            $scope.volumed = !$scope.volumed;
            $scope.volumed ? player.volume = 0 : player.volume = 1;
            // player.volume = 1; // включаем полную громкость (для выключения
            // player.volume = 0; // звука полю нужно присвоить значение “0”)
        };

        $scope.goToValue = function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $state.go('lesson.create', {id: data.schedule.id, className: data.schedule.className})
        };

        $scope.lessonRun = function () {
            $scope.timeout = true;
            ngSocket.emit('updateSchedule', {state: 1, id: $stateParams.id});
        };


        $scope.stopFight1 = false;
        $scope._learner_ = false;
        $scope.callToStudent = function (id) {
            if ($scope._learner_) {
                $scope.stopCallToStudent();
                return false;
            }
            $scope._learner_ = id;
            $scope.call = true;
            ngSocket.emit('callTimer', {date: Date.now() + 15000, id: id});
            $scope.timer1 = 15;
            var stop1 = $interval(function () {

                if ($scope.timer1 > 0) {
                    $scope.timer1 = +$scope.timer1 - 1;
                    if (!$scope.timer1) {
                        ngSocket.emit('callLearner', {id: id});
                    }
                } else {
                    $scope.stopFight1();
                }
            }, 1000);

            $scope.stopFight1 = function () {
                $scope.call = false;
                $interval.cancel(stop1);
                stop1 = undefined;
                $scope._learner_ = false;
            };
        };
        $scope.stopCallToStudent = function () {
            if ($scope.stopFight1) {
                ngSocket.emit('stopTimer', {id: $scope._learner_});
                $scope.stopFight1();
            }
        };

        ngSocket.on('videoFromLearner', function (data) {
            $scope.f.playStream({name: data.name, remoteMediaElementId: 'learnerVideo'});
        });

        ngSocket.on('restartSteamFromTeacherCam', function (data) {
            $scope.webCam = data.name;
            $scope.camerasList.every(function (nextCamera, i) {
                //если это вебкамера учителя и я ученик  повторно опубликовать поток
                if (!nextCamera.url.indexOf('teacherCam') && $scope.$state.current.name === 'lesson.learner') {
                    nextCamera.url = 'teacherCam' + data.name;
                    $scope.f.playStream({
                        name: 'teacherCam' + nextCamera.url,
                        remoteMediaElementId: 'remoteVideo' + (i ? i : '')
                    });
                    return false
                }
                return true
            })
        });

        $scope.editedTag = function () {
            $scope.tagEdit = this.tagEdit;
            ngSocket.emit('editSchedule', {id: $stateParams.id, tag: this.tagEdit});
        };


        ngSocket.on('editSchedule', function (data) {
            if(data.err) {
                alert(data.message)
            }
        });


        ngSocket.on('catchSchedule', function (data) {
            if (data.err) {
                alert(data.message);
            }
            $scope.tagEdit = data.data.tag;
        });

        $scope.lessonPause = function () {
            $scope.call = false;
            $scope.timeout = true;
            ngSocket.emit('updateSchedule', {state: 2, id: $stateParams.id});
        };
        $scope.lessonContinue = function (key) {
            $scope.classContinue = false;
            $scope.timeout = true;
            $scope.classPause = key;
            ngSocket.emit('updateSchedule', {state: 1, id: $stateParams.id});
        };
        $scope.lessonEnd = function () {
            $scope.timeout = true;
            $scope.editTag = true;
            ngSocket.emit('updateSchedule', {state: 3, id: $stateParams.id});
        };
        ngSocket.on('learnerDeparted', function (data) {
            $scope.learners.forEach(function (learner) {
                if (+learner.user.id === +data.userId) {

                    if (data.departed !== undefined && data.departed !== null) {
                        learner.departed = data.departed;
                    }
                    if (data.armUp !== undefined && data.armUp !== null) {
                        learner.armUp = data.armUp;
                    }
                    return false;
                }
            });
        });


        $scope.f = false;
        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'lesson' || $scope.$state.current.name.indexOf('lesson.learner') == 0) {
                if ($scope.camerasList && ($scope.camerasList.length || $scope.webCam)) {
                    $scope.initPlayer();
                } else {
                    vwLoading = true;
                }
            }

            //Предпросмотр
            if ($('#videoTest').length) {
                var video = $('#videoTest')[0],
                    vendorUrl = window.URL || window.webkitURL;
                navigator.getMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
                navigator.getMedia(
                    {
                        video: true,
                        audio: true
                    }
                    , function (stream) {
                        video.src = vendorUrl.createObjectURL(stream);
                        if ($scope.micAlertShow !== false && !$scope.micAlertShow) {
                            $scope.micAlertShow = true;
                        }
                    }, function (error) {
                        $scope.micAlertShow = false;
                    }
                );
            }
        });

        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'lesson') {
                // получение текущего урока
                ngSocket.emit('getSchedule', {id: +$stateParams.id});
            }
        });


        // смена окна видео
        $scope.changeVideoWindow = function (data) {
            var temp = document.getElementById('remoteVideo' + data).src;
            document.getElementById('remoteVideo' + data).src = document.getElementById('remoteVideo').src;
            document.getElementById('remoteVideo').src = temp;
            temp = $scope.camerasList[data].url;
            $scope.camerasList[data].url = $scope.camerasList[0].url;
            $scope.camerasList[0].url = temp;
        };
        $scope.$on('LastRepeaterElement', function () {
            $scope.initPlayer();
        });
    }]).controller('RecordSaved', ['$scope', function ($scope) {
        $scope.learnerSwap = function () {
            $scope.editRecordSucsessful1 = !$scope.editRecordSucsessful1;
        }
    }]).controller('LessonCreate', ['$scope', 'ngSocket', '$stateParams', '$state', function ($scope, ngSocket, $stateParams, $state) {



        if (+$stateParams.id) {
            $scope.id = +$stateParams.id;
            ngSocket.emit('getSchedule', {id: $scope.id});
        }

        ngSocket.on('catchSchedule', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.className = result.data.class == undefined ? '' : result.data.class.className;
            $scope.lessonName = result.data.lesson == undefined ? '' : result.data.lesson.lessonName;
            $scope.topic = result.data.topic;
            $scope.duration = result.data.duration;
            $scope.tag = result.data.tag;
            $scope.officeId = result.data.officeId;
            var t = result.data.durationEnd.split(':');
            $scope.durationEnd = t[0] + ':' + t[1];
        });
        var d = new Date() + '';
        var time = (d.split(' '))[4].split(':');
        $scope.time = time[0] + ':' + time[1];
        if($scope.duration !== '') {
        $scope.durationLesson = function () {
            if($scope.duration=='' || !$scope.duration || isNaN($scope.duration )){
                return $scope.durationEnd = null;
            }
            var time1 = new Date(),
                time2 = new Date(+time1 + parseInt($scope.duration) * 6e4) + '';
            var t = (time2.split(' '))[4].split(':');
                $scope.durationEnd = t[0] + ':' + t[1];
        };
        }


        $scope.$on('dropDownNewValue', function () {
            if ($scope.$state.includes('lesson.create')) {
                setTimeout(function () {
                    if (+$scope.officeId) {
                        ngSocket.emit('updateSchedule', {officeId: +$scope.officeId, id: $stateParams.id});
                    }
                }, 5);
            }
        });

        //проверка кабинета на занятность

        ngSocket.on('stopFuckingRoom', function (result) {
            $scope.busy = result.err ? 1 : 0
        });

        $scope.updateSchedule = function () {
            ngSocket.emit('updateSchedule', {
                duration: $scope.duration,
                topic: $scope.topic,
                officeId: $scope.officeId,
                id: $stateParams.id,
                tag: $scope.tag,
                durationEnd: $scope.durationEnd
            });
            if (!$scope.busy && $scope.duration && $scope.topic && +$scope.officeId) {
                $state.go('lesson', {id: $scope.id});
            }
        };

        //Предпросмотр
        $(document).ready(function () {
            if ($('#videoTest').length) {
                var video = $('#videoTest')[0],
                    vendorUrl = window.URL || window.webkitURL;
                navigator.getMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
                navigator.getMedia(
                    {
                        video: true,
                        audio: true
                    }
                    , function (stream) {
                        video.src = vendorUrl.createObjectURL(stream);
                        if ($scope.micAlertShow !== false && !$scope.micAlertShow) {
                            $scope.micAlertShow = true;
                        }
                    }, function (error) {
                        $scope.micAlertShow = false;
                    }
                );
            }
        });

    }]).controller('LessonsSchedule', ['ngSocket', '$scope', '$state', '$stateParams', 'FileUploader', function (ngSocket, $scope, $state, $stateParams, FileUploader) {

      $scope.sortByPeriod = function (key, val) {
        $scope.sr[key] = val;
        $scope.$emit('dropDownNewValue');
      }

            $scope.sendSearchRequest = {};
            // объявление переменных необходимых для сортировки и выборки
            $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
            $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
            var offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);
            var limit = 4;
            // Календарь начало
            Date.prototype.daysInMonth = function () {
                return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
            };
            $scope.buildCalendar = function () {
                var mas = [];
                var z = 0;
                var now;
                if (arguments.length) {
                    $scope.currentDate.setMonth(arguments[0]);
                    now = $scope.currentDate;
                } else {
                    now = new Date();
                }
                now.setDate(1);
                var today = now.getDay();
                var correctToday;
                if (today == 0) {
                    correctToday = 7;
                } else {
                    correctToday = today;
                }
                mas[0] = [];
                for (var bb = 0; bb < 7; bb++) {
                    mas[0][bb] = {name: (bb < correctToday - 1 ? '' : ++z)};
                }
                for (var aa = 1; aa < ((now.daysInMonth() + correctToday - 1) / 7); aa++) {
                    mas[aa] = [];
                    for (var bb = 0; bb < 7; bb++) {
                        mas[aa][bb] = {name: (z < now.daysInMonth() ? ++z : '')};
                        if (z === (new Date()).getDate()
                            && now.getMonth() === (new Date()).getMonth()
                            && now.getFullYear() === (new Date()).getFullYear()) {
                            mas[aa][bb].current = true;
                        }
                    }
                }
                $scope.testVar = mas;
                var month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
                $scope.month = month[now.getMonth()];
                $scope.currentDate = now;
            };
            $scope.previousMonth = function () {
                $scope.buildCalendar($scope.currentDate.getMonth() - 1);
            };
            $scope.nextMonth = function () {
                $scope.buildCalendar($scope.currentDate.getMonth() + 1);
            };
            // Календарь конец
            // Вывод числа по клику на число в календаре начало
            $scope.transferDate = function (name) { // сохраненяем дату в $scope
                var datestring = {};
                $scope.currentDate.setDate(name);
                datestring.date = $scope.currentDate.getDate() < 10 ? '0' + $scope.currentDate.getDate() : $scope.currentDate.getDate();
                datestring.month = $scope.currentDate.getMonth() < 9 ? '0' + ($scope.currentDate.getMonth() + 1) : $scope.currentDate.getMonth() + 1;
                datestring.year = $scope.currentDate.getFullYear() < 10 ? '0' + $scope.currentDate.getFullYear() : $scope.currentDate.getFullYear();
                $scope.transferYMD = datestring.date + '.' + datestring.month + '.' + datestring.year;
                // $scope.sendSearchRequest = $scope.transferYMD;
                $scope.sendSearchRequest.begin_ts = $scope.transferYMD;
                $scope.goToState(offset);
            };

            $scope.goToState = function () {
                offset = arguments.length ? arguments[0] : offset;
                $state.go($state.current.name, {
                    sort: JSON.stringify($scope.sortParams),
                    filter: JSON.stringify($scope.sendSearchRequest),
                    offset: offset
                });
            };
            $scope.$on('$viewContentLoading', function () {
              console.log($scope.$state.current.name);
              $scope.sendRequest();
                if ($scope.$state.current.name === 'lesson.lessonschedule' ||
              $scope.$state.current.name === 'lesson.lessonrecords') {
                    $scope.sendRequest();
                }
            });
            $scope.sendRequest = function () {
                ngSocket.emit('getScheduleList', {
                    limit: limit,
                    offset: offset,
                    filter: $scope.sendSearchRequest,
                    sort: $scope.sortParams
                });
                ngSocket.emit('getRecordList', {
                    limit: limit,
                    offset: offset,
                    filter: $scope.sendSearchRequest,
                    sort: $scope.sortParams
                });
            };

            $scope.setSort = function (key) {
                if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
                    $scope.sortParams[key] = 0;
                } else {
                    $scope.sortParams[key]++;
                }
                $scope.sendRequest();
            };
            $scope.resetData = function () {
                $scope.sendSearchRequest.begin_ts = ' ';
                $scope.goToState(offset);
            };

            // Вывод числа по клику на число в календаре конец
            $scope.sr = {};
            $scope.sr.lessonId = parseInt($scope.sendSearchRequest.lessonId);
            $scope.sr.classId = parseInt($scope.sendSearchRequest.classId);
            $scope.sr.periodId = parseInt($scope.sendSearchRequest.periodId);
            $scope.$on('$viewContentLoading', function () {
                $scope.$on('dropDownNewValue', function () {
                  console.log($scope.$state.current.name);
                    if ($scope.$state.current.name === 'lesson.lessonschedule' ||
                  $scope.$state.current.name === 'lesson.lessonrecords') {
                        setTimeout(function () {
                            if ($scope.sr.lessonId !== $scope.sendSearchRequest.lessonId) {
                                $scope.sendSearchRequest.lessonId = +$scope.sr.lessonId;
                                $scope.goToState();
                            }
                            if ($scope.sr.classId !== $scope.sendSearchRequest.classId) {
                                $scope.sendSearchRequest.classId = +$scope.sr.classId;
                                $scope.goToState();
                            }
                            if ($scope.sr.periodId !== $scope.sendSearchRequest.periodId) {
                                $scope.sendSearchRequest.periodId = +$scope.sr.periodId;
                                $scope.goToState();
                            }
                        }, 5);
                    }
                });
            });
            ngSocket.emit('getLessonList', {});
            ngSocket.emit('getClassList', {});

            ngSocket.on('lessonsList', function (data) {
                if (data.err) {
                    alert(data.message);
                }
                $scope.lesson = {0: 'Все предметы'};
                data.lessons.rows.forEach(function (elem) {
                    $scope.lesson[elem.id] = elem.lessonName;
                });
            });
            ngSocket.on('classList', function (data) {
                if (data.err) {
                    alert(data.message);
                }
                $scope.class = {0: 'Все классы'};
                data.data.rows.forEach(function (elem) {
                    $scope.class[elem.id] = elem.className;
                });
            });

            ngSocket.on('scheduleList', function (data) {
                if (data.err) {
                    alert(data.err);
                    return false;
                }
                $scope.schedules = data.data.rows;
                $scope.stateLess = data.data.state == 3 ? 'Просмотреть' : 'Войти';
                var countPages = Math.round((data.data.count) / (limit - 1));
                $scope.pagination = [];
                for (var i = 0; i < countPages; i++) {
                    $scope.pagination.push({
                        num: i,
                        offset: limit * i,
                        active: ((!offset && i == 0) || offset == (limit * i))
                    });
                }
            });

            // загрузка аватарки на сервер
            // angular-file-upload
            // https://github.com/nervgh/angular-file-upload/wiki/Module-API#directives
            var uploader = $scope.uploader = new FileUploader({
                url: '/api/upload/avatar',
                queueLimit: 1,
                removeAfterUpload: true,
                onSuccessItem: function (item, response, status, headers) {
                    alert('Файл загружен');
                    $scope.avatar = true;
                }
            });

            ngSocket.on('fileList', function (result) {


                if (result.err) {
                    alert(result.message);
                    return false;
                }
                console.log(result);
                if (result.files.length) {
                  for (var i = 0; i < $scope.records.length; i++) {
                    console.log($scope.records[i].scheduleId, 'rec');
                    console.log(result.files[0].schedules[0].id, 'result');
                    if ($scope.records[i].scheduleId !== result.files[0].schedules[0].id) continue;
                    $scope.records[i].filelist = result;
                    break;
                  }
                }
            });

            // FILTERS
            uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });
            ngSocket.on('recordList', function (data) {
                if (data.err) {
                    alert(data.message);
                }
                $scope.records = data.data.rows;
                $scope.records.forEach(function (rec) {
                  ngSocket.emit('getFileList', {scheduleId: rec.scheduleId});
                })
            });


            $scope.sendFilterSchedule = function (e) {
                if (e.keyCode === 13) {
                    ngSocket.emit('getScheduleList', {
                        limit: limit,
                        offset: offset,
                        filter: $scope.sendSearchRequest,
                        sort: $scope.sortParams
                    });
                }
            };
            $scope.sendFilterRecord = function (e) {
                if (e.keyCode === 13) {
                    ngSocket.emit('getRecordList', {
                        limit: limit,
                        offset: offset,
                        filter: $scope.sendSearchRequest,
                        sort: $scope.sortParams
                    });
                }
            };

        }]).controller('LessonLearner', ['ngSocket', '$rootScope', '$scope', '$state', '$stateParams', '$interval', function (ngSocket, $rootScope, $scope, $state, $stateParams, $interval) {

        ngSocket.emit('updateLearner', {id: $stateParams.id});
        $scope.closed = function () {
            ngSocket.emit('updateLearner', {id: $scope.id, scheduleId: null});
        };
        $scope.departed = function (key) {
            $scope.btnDeparted = key;
            ngSocket.emit('updateLearner', {departed: key, id: $scope.id, scheduleId: +$stateParams.id});
        };
        $scope.stopF = null;
        $scope.stopFight = function () {
            $interval.cancel($scope.stopF);
            $scope.plash = false;
        };
        $scope.timer = null;
        ngSocket.on('callTimerFromTeacher', function (data) {
            $scope.plash = true;
            var time = (+data.date - Date.now()) / 1000;
            $scope.timer = time.toFixed();
            $scope.stopF = $interval(function () {

                if ($scope.timer > 0) {
                    $scope.timer = +$scope.timer - 1;
                } else {
                    $scope.stopFight();
                }
            }, 1000);
        });

        ngSocket.on('stopTimer', function () {
            if ($scope.stopFight) {
                $scope.stopFight();
            }
        });

            ngSocket.emit('updateSchedule', {id: $stateParams.id});
            ngSocket.on('catchSchedule', function (result) {
                if (result.err) {
                    alert(result.message);
                    return false;
                }
                // получение списка камер подключенных к этому уроку в этом кабинете
                if(result.data.state === 1) {
                    result.data.officeId > 0
                    && ngSocket.emit('getCamerasList', {id: (+result.data.officeId)});
                }
                });
        $scope.armUp = function (key) {
            $scope.btnArmUp = key;
            ngSocket.emit('updateLearner', {armUp: key, id: $scope.id, scheduleId: +$stateParams.id});
        };
        ngSocket.emit('getSchedule', {id: $stateParams.id});
        ngSocket.on('learnerUpdate', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.id = data.data.id;
            $scope.btnDeparted = data.data.departed;
            $scope.btnArmUp = data.data.armUp
        });

        ngSocket.on('callFromTeacher', function (data) {
            var name = 'camUser' + $rootScope.user.id + Date.now();
            $scope._teacher.setId(data.teacherId);
            $scope.f.publishStream({name: name, record: false});
        });

        ngSocket.on('catchSchedule', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.teacher = data.data.user.firstName + ' '
                + data.data.user.lastName + ' '
                + data.data.user.patronymic;
            $scope.topic = data.data.topic;
            $scope.state = data.data.state;
        });

        ngSocket.on('scheduleUpdate', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.classRun = true;
        });
        $(document).ready(function () {
            //Предпросмотр перед трансляцией
            if ($('#localVideo').length) {
                var video = $('#localVideo')[0],
                    vendorUrl = window.URL || window.webkitURL;
                navigator.getMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
                navigator.getMedia(
                    {
                        video: true,
                        audio: true
                    }
                    , function (stream) {
                        video.src = vendorUrl.createObjectURL(stream);
                    }, function (error) {

                    }
                );
            }
        })
    }])
});
