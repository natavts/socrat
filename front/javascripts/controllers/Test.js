define(['./module', 'jquery'], function (controllers, $) {
  'use strict';

  controllers.controller('Test', ['$scope', '$stateParams', 'ngSocket', '$state', function ($scope, $stateParams, ngSocket, $state) {

    $scope.status = {
        isCustomHeaderOpen: false,
        isFirstOpen: true,
        isFirstDisabled: false
      };

    $scope.sortByPeriod = function (val) {
      $scope.sr.periodId = val;
      $scope.$emit('dropDownNewValue');
    }



$scope.goPreviousPage = function () {
    if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
    else $state.go('test', {
        sort: JSON.stringify($scope.sortParams),
        filter: JSON.stringify($scope.sendSearchRequest),
        offset: offset - limit
    });
};
$scope.goNextPage = function () {
    if (offset >= ($scope.pagination.length - 1) * limit) return false;
    $state.go('test', {
        sort: JSON.stringify($scope.sortParams),
        filter: JSON.stringify($scope.sendSearchRequest),
        offset: offset + limit
    });
};

// Календарь начало
Date.prototype.daysInMonth = function () {
    return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
};

function getLastDayOfMonth(year, month) { // Функция, которая возвращает последний день месяца
    var date = new Date(year, month + 1, 0);
    return date.getDate();
}

$scope.buildCalendar = function () {
    //var a = 7, b = 4;
    var mas = [];
    var z = 0;
    var now;

    if (arguments.length) {
        $scope.currentDate.setMonth(arguments[0]);
        now = $scope.currentDate;
    } else {
        now = new Date();
    }

    now.setDate(1);
    var today = now.getDay();
    var correctToday;

    if (today == 0) {
        correctToday = 7;
    } else {
        correctToday = today;
    }

    mas[0] = [];

    for (var bb = 0; bb < 7; bb++) {
        mas[0][bb] = {name: (bb < correctToday - 1 ? '' : ++z)};

        if (z === (new Date()).getDate()
            && now.getMonth() === (new Date()).getMonth()
            && now.getFullYear() === (new Date()).getFullYear() ) {
            mas[0][bb].current = true;
        }
    }

    //for (var aa = 1; aa < ((now.daysInMonth() + correctToday - 1) / 7); aa++) {
    for (var aa = 1; aa < (now.daysInMonth() / 7); aa++) {
        mas[aa] = [];

        for (var bb = 0; bb < 7; bb++) {
            mas[aa][bb] = {name: (z < now.daysInMonth() ? ++z : '')};

            if (z === (new Date()).getDate()
                && now.getMonth() === (new Date()).getMonth()
                && now.getFullYear() === (new Date()).getFullYear() ) {
                mas[aa][bb].current = true;

                if ( z === getLastDayOfMonth(now.getFullYear(), now.getMonth()) ) {
                    mas[aa][bb].current = true;
                    break;
                }
            }
        }
    }
    $scope.testVar = mas;
    var month = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];
    $scope.month = month[now.getMonth()];
    $scope.currentDate = now;
};
$scope.previousMonth = function () {
    $scope.buildCalendar($scope.currentDate.getMonth() - 1);

};
$scope.nextMonth = function () {
    $scope.buildCalendar($scope.currentDate.getMonth() + 1);
};
// Календарь конец
// Вывод числа по клику на число в календаре начало
$scope.sendSearchRequest = {};
// объявление переменных необходимых для сортировки и выборки
$scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
$scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
let offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);
let limit = 4;

$scope.transferDate = function (name) { // сохраненяем дату в $scope
    var datestring = {};
    $scope.currentDate.setDate(name);
    datestring.date = $scope.currentDate.getDate() < 10 ? '0' + $scope.currentDate.getDate() : $scope.currentDate.getDate();
    datestring.month = $scope.currentDate.getMonth() < 9 ? '0' + ($scope.currentDate.getMonth() + 1) : $scope.currentDate.getMonth() + 1;
    datestring.year = $scope.currentDate.getFullYear() < 10 ? '0' + $scope.currentDate.getFullYear() : $scope.currentDate.getFullYear();
    $scope.sendSearchRequest.begin_ts = datestring.date + '.' + datestring.month + '.' + datestring.year;
    $scope.goToState(offset);
};

// Вывод числа по клику на число в календаре конец

// переключение кнопок "Начать урок" / "Войти в урок"
$scope.scheduleSwap = function () {
    $scope.editRecordSucsessful1 = !$scope.editRecordSucsessful1;
};
//
$scope.showNoteUpdated = $stateParams.showNoteUpdated;
$scope.sr = {};
$scope.sr.lessonId = parseInt($scope.sendSearchRequest.lessonId);
$scope.sr.classId = parseInt($scope.sendSearchRequest.classId);
$scope.sr.periodId = parseInt($scope.sendSearchRequest.periodId);
$scope.$on('$viewContentLoading', function () {

    $scope.$on('dropDownNewValue', function () {
      console.log('dropDownNewValue');
        if ($scope.$state.current.name === 'test') {
            setTimeout(function() {
                if ($scope.sr.lessonId !== $scope.sendSearchRequest.lessonId) {
                    $scope.sendSearchRequest.lessonId = +$scope.sr.lessonId;
                    $scope.goToState();
                }
                if ($scope.sr.classId !== $scope.sendSearchRequest.classId) {
                    $scope.sendSearchRequest.classId = +$scope.sr.classId;
                    $scope.goToState();
                }
                if ($scope.sr.periodId !== $scope.sendSearchRequest.periodId) {
                    $scope.sendSearchRequest.periodId = +$scope.sr.periodId;
                    $scope.goToState();
                }
            },5);
        }
    });

});

$scope.oldDate = function (d) {
    var a =  new Date(d).getTime();
    return a < Date.now();
};

ngSocket.emit('getLessonList', {});
ngSocket.emit('getClassList', {});

ngSocket.on('lessonsList', function (data) {
    if (data.err) {
        alert(data.message);
    }
    $scope.lesson = {0:'Все предметы'};
    data.lessons.rows.forEach(function (elem) {
        $scope.lesson[elem.id] = elem.lessonName;
    });
});
ngSocket.on('classList', function (data) {
    if (data.err) {
        alert(data.message);
    }
    $scope.class = {0:'Все классы'};
    data.data.rows.forEach(function (elem) {
        $scope.class[elem.id] = elem.className;
    });
});

$scope.sendRequest = function () {
    ngSocket.emit('getScheduleList', {
        limit: limit,
        offset: offset,
        filter: $scope.sendSearchRequest,
        sort: $scope.sortParams
    });
};


$scope.sendFilter = function (e) {
    if (e.keyCode === 13) {
        $scope.goToState();
    }
};
$scope.setSort = function (key) {
    if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
        $scope.sortParams[key] = 0;
    } else {
        $scope.sortParams[key]++;
    }
    $scope.sendRequest();
};
$scope.resetData = function () {
    $scope.sendSearchRequest.begin_ts = ' ';
    $scope.goToState(offset);
};

$scope.goToState = function () {
    offset = arguments.length ? arguments[0] : offset;
    $state.go($state.current.name, {
        sort: JSON.stringify($scope.sortParams),
        filter: JSON.stringify($scope.sendSearchRequest),
        offset: offset
    });
};

ngSocket.on('scheduleList', function (result) {
    if (result.err) {
        alert(result.message);
        return false
    }
    $scope.schedules = result.data.rows;
    var countPages = Math.round((result.data.count + 1) / (limit - 1));
    $scope.pagination = [];
    for (var i = 0; i < countPages; i++) {
        $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
    }
});
$scope.$on('$viewContentLoading', function () {
    if ($scope.$state.current.name === 'test') {
        $scope.sendRequest();
    }
});


ngSocket.on('createSchedule', function (result) {
    if (result.err) {
        alert(result.message);
        return false;
    }
});

ngSocket.on('deleteSchedule', function (result) {
    if (result.err) {
        alert(result.message);
        return false;
    }
    $scope.sendRequest();
});

$scope.deleteSchedule = function () {
    ngSocket.emit('deleteSchedule', {id: +$scope.deleteId})
}


ngSocket.on('userListSelected', function (users) {
    if(users.err) {
        alert(users.message);
    }
    $scope.users = {};
    users.users.rows.forEach(function (elem) {
        $scope.users[elem.id] = elem.firstName + ' ' + elem.lastName + ' ' + elem.patronymic
    });
});

$scope.choosedSchedule = {};

$scope.chooseSchedule = function (id) {
  $scope.id = +id;
  ngSocket.emit('getSchedule', {id: $scope.id});
}

ngSocket.on('catchSchedule', function (result) {
    if (result.err) {
        alert(result.message);
        return false;
    }
    var begin_ts_arr = (result.data.begin_ts.split('T'))[0].split('-');
    $scope.choosedSchedule = JSON.parse(JSON.stringify(result.data));
    $scope.choosedSchedule.begin_ts = begin_ts_arr[2] + '.' + begin_ts_arr[1] + '.' + begin_ts_arr[0];
    $scope.choosedSchedule.lessonId = +$scope.choosedSchedule.lessonId;
    $scope.choosedSchedule.classId = +$scope.choosedSchedule.classId;
});

ngSocket.on('editSchedule', function (data) {
    if (data.err) {
        alert(data.message);
        return false;
    }

});

//select выбора класса
ngSocket.on('classList', function (result) {
    if (result.err) {
        alert(result.message);
        return false
    }
    $scope.classes = result.data.rows;
});
//select выбора придмета
ngSocket.on('lessonsList', function (result) {
    if (result.err) {
        alert(result.message);
        return false
    }
    $scope.lessons = result.lessons.rows;
});

$scope.editSchedule = function () {
    var temp = $scope.choosedSchedule.begin_ts;
    var begin_ts_arr_new = (temp.split('.'));
    var tempTime = $scope.choosedSchedule.time.split(':');
    var time = tempTime[0] + ':' + tempTime[1] + ':' + '00.000';
    $scope.choosedSchedule.begin_ts = begin_ts_arr_new[2] + '-' + begin_ts_arr_new[1] + '-' + (+begin_ts_arr_new[0]) + 'T' + time;
    console.log(new Date(),$scope.choosedSchedule.begin_ts);
    ngSocket.emit('editSchedule', {
        id: $scope.id,
        classId: $scope.choosedSchedule.classId,
        lessonId: $scope.choosedSchedule.lessonId,
        time: $scope.choosedSchedule.time,
        begin_ts: $scope.choosedSchedule.begin_ts
    });

    $scope.sendRequest();

}


  }]);
});
