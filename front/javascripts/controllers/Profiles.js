define(['./module', 'jquery'], function (controllers, $) {
  'use strict';

  controllers.controller('Profiles', ['$scope', '$http', '$rootScope', '$state', function ($scope, $http, $rootScope, $state) {

  }]).controller('ProfilesEdit', ['$scope', '$http', '$rootScope', '$stateParams', function ($scope, $http, $rootScope, $stateParams) {
    $scope.edit = $stateParams.edit;
  }]).controller('ProfilesSave', ['$scope', '$http', '$rootScope', '$stateParams', function ($scope, $http, $rootScope, $stateParams) {
    $scope.save = $stateParams.save;
  }])
});
