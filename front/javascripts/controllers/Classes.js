define(['./module','jquery'],function(controllers,$){
    'use strict';
    
    controllers.controller('Classes',['$scope','ngSocket','$stateParams','$state', function($scope, ngSocket,$stateParams, $state){

        $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
        var offset = parseInt($stateParams.offset==null?0:$stateParams.offset);
        var limit = 4;

        $scope.goPreviousPage = function() {
            if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
            else $state.go('classes', {sort: JSON.stringify($scope.sortParams), filter : JSON.stringify($scope.sendSearchRequest), offset:offset-limit});
        };

        $scope.goNextPage = function () {
            if (offset >= ($scope.pagination.length-1)*limit) return false;
            $state.go('classes', {sort: JSON.stringify($scope.sortParams), filter : JSON.stringify($scope.sendSearchRequest), offset:offset+limit});
        };
        
        function sendRequest() {
            ngSocket.emit('getClassList', {
                limit: limit,
                offset: offset,
                filter: $scope.sendSearchRequest,
                sort: $scope.sortParams});
        }

        function goToState() {
            offset = arguments.length?arguments[0]:offset;
            $state.go('classes', {sort: JSON.stringify($scope.sortParams), filter: JSON.stringify($scope.sendSearchRequest),offset:offset});
            offset = arguments.length ? arguments[0] : offset;
            $state.go('classes', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        }

        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'classes') {
                sendRequest();
            }
        });

        $scope.sendFilter = function (e) {
            if (e.keyCode === 13) {
                goToState();
            }
        };

        $scope.setSort = function (key) {
            if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
                $scope.sortParams[key] = 0;
            } else {
                $scope.sortParams[key]++;
            }
            sendRequest();
        };

        // получение списка строк для таблицы
        ngSocket.on('classList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.classList = result.data.rows;
            var countPages = Math.round(result.data.count / (limit - 1));
            $scope.pagination = [];
            for (var i = 0; i < countPages; i++) {
                $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
            }
        });

        // создание класса
        $scope.createCl = $stateParams.className;
        ngSocket.on('createClass', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.createCl = result.newClass.className;
            sendRequest();
        });

        // удаление класса
        ngSocket.on('deleteClass', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            sendRequest();
        });

        $scope.goToPage = function (offset) {
            goToState(offset);
        };

        $scope.deleteClass = function () {
            ngSocket.emit('deleteClass', {id: +$scope.deleteId});
        };

        $scope.sortLink = $stateParams.name;
    }]).controller('ClassesCreate', ['$scope', 'ngSocket', function ($scope, ngSocket) {
        $scope._createClass = {
            className : '',
            commentaries : ''
        };
        $scope.switchingBetweenFunctionsCreateClassAndSaveAndContinue = 0; // вызов/переключение между
        // функциями createClass - 0 и saveAndContinue - 1
        $scope.createClass = function () {
            $scope.switchingBetweenFunctionsCreateClassAndSaveAndContinue = 0;
            ngSocket.emit('createClass', {className: $scope._createClass.className, commentaries: $scope._createClass.commentaries});
        };
        //
        $scope.saveAndContinue = function () {
            $scope.switchingBetweenFunctionsCreateClassAndSaveAndContinue = 1;
            ngSocket.emit('createClass', {className: $scope._createClass.className, commentaries: $scope._createClass.commentaries});
        };
        //

        ngSocket.on('createClass', function (result) {
            if (!result.err) {
                $scope.createCl1 = result.newClass.className;
                if($scope.switchingBetweenFunctionsCreateClassAndSaveAndContinue){
                    $scope._createClass.className = '';
                    $scope._createClass.commentaries = '';
                    // ввести обнуление параметров ввода и добавить выскакивающую плашку
                    return false;
                }
            }
        });
    }]).controller('ClassesView', ['$scope', 'ngSocket', '$stateParams', '$state', function ($scope, ngSocket, $stateParams, $state) {
        $scope.classId = $stateParams.id;
        ngSocket.emit('getClass', {id: $scope.classId});
        ngSocket.on('catchClass', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            if (+$stateParams.newClass) {
                $scope.save = true;
            }
            $scope.className = data.data.className;
            $scope.commentaries = data.data.commentaries;
        });

    }]).controller('ClassesEdit', ['$scope', 'ngSocket', '$stateParams', '$state', function ($scope, ngSocket, $stateParams, $state) {
        $scope.save = false;
        $scope.classId = $stateParams.id;
        ngSocket.emit('getClass', {id: $scope.classId});
        ngSocket.on('catchClass', function (data) {
            if (data.err) {
                alert(data.message);
                return false
            }
            $scope.className = data.data.className;
            $scope.commentaries = data.data.commentaries;
        });
        ngSocket.on('classEdited', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.save = true;

            $state.go('classes.classview', {id: data.classes.classId, newClass: 1})
        });
        $scope.editedClass = function () {
            ngSocket.emit('editedClass', {
                className: $scope.className,
                commentaries: $scope.commentaries,
                id: $scope.classId
            });
        }
    }])
});