/**
 * Created by Роман on 08.12.2014.
 */
define([
    'angular',
    'jquery',
    'bstrap',
    'FP',
    'angularFileUpload',
    'ui.bootstrap'
    ], function (ng) {
    'use strict';
    return ng.module('app.controllers', []);
});
