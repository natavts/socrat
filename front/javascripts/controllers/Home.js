define(['./module'],function(controllers){
    'use strict';
    controllers.controller('Home',['$scope','$http', '$rootScope', '$state', '$sessionStorage', 'ngSocket', function($scope,$http,$rootScope,$state, $sessionStorage,ngSocket){
        if($sessionStorage.role!==undefined && $sessionStorage.role){
            go($sessionStorage.role);
        }
        $scope.role = $sessionStorage.role;
        $scope.login = function () {
            $http.post('/api/users/login',{ username: $scope.username, password: $scope.password }).then(
                //ok
                function(response){
                    var data = response.data;
                    if(data.err){
                        $scope.wrongPassword = true;
                        $rootScope.user = null;
                        return false;
                    }
                    $sessionStorage.user_id = data.doc.id;
                    $sessionStorage.firstName = data.doc.firstName;
                    $sessionStorage.lastName = data.doc.lastName;
                    $sessionStorage.patronymic = data.doc.patronymic;
                    $sessionStorage.auth = {login:1};
                    $sessionStorage.role = +data.doc.role;
                    location.reload();
                },
                //fatalErr
                function(){}
            );
        };
        $scope.wrongPasswordClear = function(){
            $scope.wrongPassword = false;
        };
        function go(role) {
            switch (role){
                case 1://Суперадмин
                    $scope.$state.go('superadmin-users');
                    break;
                case 2://Админ
                    $scope.$state.go('users');
                    break;
                case 3://Учитель
                    $scope.$state.go('schedule');
                    break;
                case 4://Ученик
                    $scope.$state.go('lesson.lessonschedule');
                    break;
            }
        }
    }])
});
