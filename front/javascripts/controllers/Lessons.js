define(['./module', 'jquery'], function (controllers, $) {
    'use strict';

    controllers.controller('Lessons', ['$scope', '$state', 'ngSocket', '$stateParams', function ($scope, $state, ngSocket, $stateParams) {
        $scope.sendSearchRequest = {};

        // объявление переменных необходимых для сортировки и выборки
        $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
        var offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);

        // количество выводимых строк в таблице
        var limit = 4;

        $scope.goPreviousPage = function() {
            if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
            else $state.go('lessons', {sort: JSON.stringify($scope.sortParams), filter : JSON.stringify($scope.sendSearchRequest), offset:offset-limit});
        };

        $scope.goNextPage = function () {
            if (offset >= ($scope.pagination.length-1)*limit) return false;
            $state.go('lessons', {sort: JSON.stringify($scope.sortParams), filter : JSON.stringify($scope.sendSearchRequest), offset:offset+limit});
        };

        //обновление списка в таблице
        function sendRequest() {
            ngSocket.emit('getLessonList', {
                limit: limit,
                offset: offset,
                filter: $scope.sendSearchRequest,
                sort: $scope.sortParams
            });
        }

        $scope.sendFilter = function (e) {
            if (e.keyCode === 13) {
                sendRequest();
            }
        };
        
        // получение списка строк для таблицы
        ngSocket.on('lessonsList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.lessonsList = result.lessons.rows;
            var countPages = Math.round(result.lessons.count / (limit - 1));
            $scope.pagination = [];
            for (var i = 0; i < countPages; i++) {
                $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
            }
        });

        $scope.goToPage = function (offset) {
            goToState(offset);
        };

        function goToState() {
            offset = arguments.length ? arguments[0] : offset;
            $state.go('lessons', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        }

        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'lessons') {
                sendRequest();
            }
        });

        // ngSocket.on('lessonsList', function (result) {
        //     $scope.lessonsList = result.lessons.rows;
        // });

        ngSocket.on('lessonCreated', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
        });

        ngSocket.on('lessonDeleted', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            sendRequest();
        });

        $scope.deleteLesson = function () {
            ngSocket.emit('deleteLesson', {lessonId: +$scope.deleteId});
        };

        // сортировка строк таблицы по возрастанию\убыванию
        $scope.setSort = function (key) {
            if ($scope.sortParams[key] >= 2 || $scope.sortParams[key] == null) {
                $scope.sortParams[key] = 0;
            } else {
                $scope.sortParams[key]++;
            }
            sendRequest();
        };

    }]).controller('LessonsCreate', ['$scope', 'ngSocket', '$state', function ($scope, ngSocket, $state) {
        $scope._lessonName = {
            lessonName : '',
            description : ''
        };
        $scope.switchingBetweenFunctionsPopoAndSaveAndContinue = 0; // вызов/переключение между
        // функциями popo - 0 и saveAndContinue - 1
        $scope.popo = function () {
            $scope.switchingBetweenFunctionsPopoAndSaveAndContinue = 0;
            ngSocket.emit('createLesson', {lessonName: $scope.lessonName, description: $scope.description});
            $scope.goToLessonView = arguments.length ? true : false;
        };
        
        $scope.saveAndContinue = function () {
            $scope.switchingBetweenFunctionsPopoAndSaveAndContinue = 1;
            ngSocket.emit('createLesson', {lessonName: $scope.lessonName, description: $scope.description});
        }; 

        ngSocket.on('lessonCreated', function (result) {
            if (!result.err) {
                $scope.lessonName = null;
                $scope.description = null;
                $scope.createAndContinue = result.newLesson.lessonName;
                if ($scope.switchingBetweenFunctionsPopoAndSaveAndContinue){
                    $scope.showTheWindowOnSuccess = 1;
                    $scope._lessonName.className = '';
                    $scope._lessonName.commentaries = '';
                    // ввести обнуление параметров ввода и добавить выскакивающую плашку
                    return false;
                }
                if ($scope.goToLessonView) {
                    $state.go('lessons.view', {id: result.newLesson.lessonId, newLesson: 1})
                }
            }
        });

    }]).controller('LessonView', ['$scope', '$stateParams', 'ngSocket', function ($scope, $stateParams, ngSocket) {
        $scope.lessonId = $stateParams.id;
        ngSocket.emit('getLesson', {lessonId: $scope.lessonId});
        ngSocket.on('catchLesson', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            if (+$stateParams.newLesson) {
                $scope.save = true;
            }
            $scope.lessonName = data.lesson.lessonName;
            $scope.description = data.lesson.description;
        });

        //редактирование предмета
    }]).controller('LessonEdit', ['$scope', '$stateParams', 'ngSocket', '$state', function ($scope, $stateParams, ngSocket, $state) {
        $scope.lessonId = $stateParams.id;
        ngSocket.emit('getLesson', {lessonId: $scope.lessonId});
        ngSocket.on('catchLesson', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            if (+$stateParams.newLesson) {
                $scope.save = true;
            }
            $scope.lessonName = data.lesson.lessonName;
            $scope.description = data.lesson.description;
        });
        $scope.saveEditedLesson = function () {
            ngSocket.emit('saveEditedLesson', {
                lessonName: $scope.lessonName,
                description: $scope.description,
                lessonId: $scope.lessonId
            });
            ngSocket.on('lessonEdited', function (data) {
                if (data.err) {
                    alert(data.message);
                    return false;
                }
                $state.go('lessons.view', {id: data.lesson.lessonId, newLesson: 1});
            });
        };
    }]);
});
