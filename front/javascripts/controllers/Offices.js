/**
 * Created by emmtech on 08.06.16.
 */
define(['./module', 'jquery'], function (controllers, $) {
    'use strict';
    controllers.controller('Office', ['$stateParams', '$scope', 'ngSocket', '$state', function ($stateParams, $scope, ngSocket, $state) {

        // удаление кабинета
        
        $scope.deleteOffice = function () {
            ngSocket.emit('deleteOffice', {id: +$scope.deleteId});
        };

        ngSocket.on('officeDeleted', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            $scope.sendRequest();
        });

        $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
        let offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);
        let limit = 4;

        $scope.sendRequest= function () {
            ngSocket.emit('getOfficeList', {
                limit: limit,
                offset: offset,
                filter: $scope.sendSearchRequest,
                sort: $scope.sortParams
            });
        };



        $scope.setSort = function (key) {
            if ($scope.sortParams[key] >= 2 || $scope.sortParams == null) {
                $scope.sortParams[key] = 0;
            } else {
                $scope.sortParams[key]++;
            }
            $scope.sendRequest();
        };

        $scope.sendFilter = function (e) {
            if (e.keyCode === 13) {
                $scope.goToState();
            }
        };
        
        $scope.goToState = function () {
            offset = arguments.length ? arguments[0] : offset;
            $state.go('offices', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        };

        ngSocket.on('officeList', function (result) {
            if (result.err) {
                alert(result.message);
                return false
            }
            console.log(result);
            $scope.officeList = result.data.rows;
            var countPages = Math.round(result.data.count / (limit - 1));
            $scope.pagination = [];
            for (var i = 0; i < countPages; i++) {
                $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
            }
        });
        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'offices') {
                $scope.sendRequest();
            }
            if ($scope.$state.$current.name === 'offices.create') {
                ngSocket.emit('getLessonList', {});
                ngSocket.on('lessonsList', function (result) {
                    if (result.err) {
                        alert(result.message);
                        return false;
                    }
                    $scope.lessons = result.lessons.rows;
                });
            }
        });

        ngSocket.on('officeCreate', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
        });

        // пагинация
        $scope.goPreviousPage = function () {
            if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
            else $state.go('offices', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset - limit
            });
        };

        $scope.goNextPage = function () {
            if (offset >= ($scope.pagination.length - 1) * limit) return false;
            $state.go('offices', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset + limit
            });
        };

    }])
    .controller('OfficeCreate', ['ngSocket', '$scope', '$state', function (ngSocket, $scope, $state) {

        $scope.lessonId = 'Выберите предмет';
        ngSocket.emit('getCamerasList', {});
        ngSocket.on('lessonsList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.lesson = {};
            result.lessons.rows.forEach(function (elem) {
                $scope.lesson[elem.id] = elem.lessonName;
            });
        });
        ngSocket.on('camerasList', function (result) {
            if (result.err) {
                alert(result.message);
                return false
            }
            $scope.cameras = {};
            result.data.rows.forEach(function (elem) {
                if(+elem.officeId === 0){
                    $scope.cameras[elem.id] = elem.name;
                }
            });
        });


        $scope.newCams = [];
        $scope.addCam = function () {
            $scope.newCams.push({camName: '', url_cam: ''});
        };

        $scope.switchingBetweenFunctionsCreateOfficeAndSaveAndContinue = 0; // вызов/переключение между
        // функциями createOffice - 0 и saveAndContinue - 1

        $scope.createOffice = function () {
            $scope.switchingBetweenFunctionsCreateOfficeAndSaveAndContinue = 0;
            ngSocket.emit('createOffice', {
                number: $scope.number,
                lessonId: $scope.lessonId,
                cameras: $scope.newCams,
                webCam: $scope.webCam
            });
        };

        $scope.saveAndContinue = function () {
            $scope.switchingBetweenFunctionsCreateOfficeAndSaveAndContinue = 1;
            ngSocket.emit('createOffice', {
                number: $scope.number,
                lessonId: $scope.lessonId,
                cameras: $scope.newCams,
                webCam: $scope.webCam
            });
        };
        //
        ngSocket.on('createOffice', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            } else {
                if($scope.switchingBetweenFunctionsCreateOfficeAndSaveAndContinue){
                    $scope.showTheWindowOnSuccess = 1;
                    $scope.number = '';
                    $scope.lessonId = 'Выберите предмет';
                    // ввести обнуление параметров ввода и добавить выскакивающую плашку
                    return false;
                }
                $state.go('offices', {
                    number: result.number,
                    lessonId: result.lessonId
                });
            }
        })
    }])
    .controller('OfficesEdit', ['$scope', 'ngSocket', '$stateParams', '$state', function ($scope, ngSocket, $stateParams, $state) {

        $scope.newCams = [];
        $scope.addCam = function () {
            $scope.newCams.push({camName: '', url_cam: ''});
        };
        
        $scope.officeId = $stateParams.id;
        $scope.save = $stateParams.save;

        ngSocket.emit('getCamerasList', {});
        ngSocket.on('camerasList', function (result) {
            if (result.err) {
                alert(result.message);
                return false
            }
            $scope.camerasConnectedByCurrentOffice = [];

            result.data.rows.forEach(function (item) {
                if (item.officeId == $stateParams.id){
                    $scope.camerasConnectedByCurrentOffice.push(item);
                }
            });
        });

        ngSocket.emit('getOffice', {id: $scope.officeId});
        ngSocket.on('catchOffice', function (data) {
            if (data.err) {
                alert(data.message);
                return false
            }
            $scope.officeRow = data.data;
            $scope.officeRow.webCam = ($scope.officeRow.webCam?true:null);
            $scope.officeRow.lessonId = $scope.officeRow.lessonId+'';
        });
        ngSocket.on('officeEdited', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            // ngSocket.emit('getCamerasList', {});
            $state.go('offices.data', {id: $scope.officeId, save: true});
        });

        ngSocket.emit('getLessonList', {});
        ngSocket.on('lessonsList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.lessons = result.lessons;
            $scope.lessons.rows.forEach(function (item) {
                if (item.id == $scope.officeRow.lessonId){
                    $scope.lessonForCurrentClass = item;
                }
            });
        });

        $scope.editedOffice = function () {
            ngSocket.emit('editOffice', {
                editedOfficeNumber: $scope.officeRow.number,
                id: $scope.officeRow.id,
                editedLessonId: $scope.officeRow.lessonId,
                cameras: $scope.newCams,
                webCam: $scope.officeRow.webCam
            });
        };
        
        $scope.deleteCam = function (camId) {
            ngSocket.emit('deleteCam', {
                // id: $scope.officeRow.id,
                id: camId
            });
        };
        ngSocket.on('cameraDeleted', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            ngSocket.emit('getCamerasList', {});
        });

        //
        $scope.schedule = {
            lessonId :'Выберите предмет',
            cameraId : 'Выберите камеру'
        };
        //запрос списка классов и предметов
        ngSocket.emit('getLessonList', {});
        ngSocket.emit('getCameraList', {});

        ngSocket.on('lessonsList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.lesson = {};
            result.lessons.rows.forEach(function (elem) {
                $scope.lesson[elem.id] = elem.lessonName;
            });
        });

        ngSocket.on('camerasList', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.cameras = {};
            result.data.rows.forEach(function (elem) {
                if(+elem.officeId === 0){
                    $scope.cameras[elem.id] = elem.name;
                }
            });
        });

        $scope.createSchedule = function () {
            ngSocket.emit('createSchedule', $scope.schedule)
        };

        ngSocket.on('createSchedule', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $state.go('schedule', {});
        });
        //

    }])
});