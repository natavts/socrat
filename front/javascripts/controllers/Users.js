define(['./module', 'jquery'], function (controllers, $) {
    'use strict';
    controllers.controller('Users', ['$scope', '$stateParams', 'ngSocket', '$state', 'FileUploader', function ($scope, $stateParams, ngSocket, $state, FileUploader) {

        // заугрузка CSV на сервер
        // angular-file-upload
        // https://github.com/nervgh/angular-file-upload/wiki/Module-API#directives
        // $scope.rowCount = 0;
        var uploader = $scope.uploader = new FileUploader({
            url: '/api/upload/userCSV',
            autoUpload: true,
            removeAfterUpload: true,
            queueLimit: 1,
            onSuccessItem (item, response, status, headers){
                alert('Файл загружен');
                $scope.CSVParsedFile = response;
            }
        });
        // FILTERS
        uploader.filters.push({
            name: 'CSVFilter',
            fn: function (item) {
                return true;
            }
        });


        $scope.addUsersFromCSV = function () {
            var data = {
                CSVParsedFile: $scope.CSVParsedFile,
                roleId: $scope.role,
                importParams: $scope.importParams,
                classSelected: $scope.classSelected
            };
            ngSocket.emit('createNewUserFromCSV', data);
        };

        ngSocket.on('createCSVReport', function (result) {
            $scope.countOfRenewedRows = result.renewedRows;
            $scope.countOfCreatedRows = result.createdRows;
        });

        $scope.sendSearchRequest = $scope.$stateParams.filter ? JSON.parse($scope.$stateParams.filter) : {};
        $scope.sortParams = $scope.$stateParams.sort ? JSON.parse($scope.$stateParams.sort) : {};
        var offset = parseInt($stateParams.offset == null ? 0 : $stateParams.offset);
        var limit = 4;

        $scope.goPreviousPage = function () {
            if ($stateParams.offset == 0 || $stateParams.offset == undefined) return;
            else $state.go('users', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset - limit
            });
        };

        $scope.goNextPage = function () {
            if (offset >= ($scope.pagination.length - 1) * limit) return false;
            $state.go('users', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset + limit
            });
        };

        function sendRequest() {
            var data = {
                limit: limit,
                offset: offset,
                filter: $scope.sendSearchRequest,
                sort: $scope.sortParams
            };
            if($scope.$state.current.name === 'users'){
                data.listView = true;
            }
            if($scope.$state.current.name === 'superadmin-users') {
                data.listSa = true;
            }
            ngSocket.emit('getUserList', data);
        }
        
        $scope.sendFilters = function (e) {
            if (e.keyCode === 13) {
                $scope.goToStates();
            }
        };
        $scope.setSort = function (key) {
            if ($scope.sortParams[key] == undefined || $scope.sortParams[key] == null || $scope.sortParams[key] >= 2) {
                $scope.sortParams[key] = 0;
            } else {
                $scope.sortParams[key]++;
            }
            sendRequest();
        };
        $scope.goToStatesSa = function () {
            offset = arguments.length ? arguments[0] : offset;
            $state.go('superadmin-users', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        };
        $scope.goToStates = function () {

            offset = arguments.length ? arguments[0] : offset;
            $state.go('users', {
                sort: JSON.stringify($scope.sortParams),
                filter: JSON.stringify($scope.sendSearchRequest),
                offset: offset
            });
        };
        $scope.$on('$viewContentLoading', function () {
            if ($scope.$state.current.name === 'users' || $scope.$state.current.name === 'superadmin-users') {
                sendRequest()
            }
        });


        ngSocket.on('userListSelected', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.userList = result.users.rows;
            var countPages = Math.round(result.users.count / (limit - 1));
            $scope.pagination = [];
            for (var i = 0; i < countPages; i++) {
                $scope.pagination.push({num: i, offset: limit * i, active: ((!offset && i==0) || offset == (limit * i))});
            }
        });

        ngSocket.on('userDeleted', function (data) {
            if (data.err) {
                alert(data.message);
                return false;
            }
            sendRequest();
        });
        $scope.deleteUser = function () {
            ngSocket.emit('deleteUser', {id: +$scope.deleteId});
        };

        $scope.$on('$viewContentLoading', function () {
            if (!$scope.$state.includes('superadmin-users')) {
                //запрос списка классов и предметов
                ngSocket.emit('getClassList', {});
            }
        });

        $scope.classSelected = 'Выберите класс';
        ngSocket.on('classList', function (result) {
            console.log(result);
            if (result.err) {
                alert(result.message);
                return false;
            }
            $scope.class = {};
            result.data.rows.forEach(function (elem) {
                $scope.class[elem.id] = elem.className;
            });
        });

        $scope.createSchedule = function () {
            ngSocket.emit('createSchedule', $scope.schedule)
        };

        ngSocket.on('createSchedule', function (result) {
            if (result.err) {
                alert(result.message);
                return false;
            }
            $state.go('schedule', {});
        });
        //

    }]).controller('UserCreate', ['$scope', '$stateParams', 'ngSocket', function ($scope, $stateParams, ngSocket) {

        // получение списка ролей для пользователя
        ngSocket.emit('getUserRoles', {});
        ngSocket.on('rolesSelected', function (data) {
            if (data.err) {
                alert(data.message)
            }
            $scope.roles = {};
            data.roles.forEach(function (elem) {
                $scope.roles[elem.id] = elem.description;
            });

        });


        ngSocket.on('catchSchedule', function (data) {
            if (data.err) {
                alert(data.message);
            }
        });
        $scope._createdUser = {
            init: function () {
                this.email = '@mail.com';
                this.password = '';
                this.firstName = '';
                this.lastName = '';
                this.patronymic = '';
                this.phone = '';
                this.introduce = '';
                this.implementing = {text: '', value: 0};
                this.numberOfClasses = 1;
                this.roleId = 'Выбрать роль';
                this.classId = 'Выбрать класс';

            }
        };
        $scope._createdUser.init();
        var userId = ($stateParams.id ? +$stateParams.id : 0);

        if (userId) {
            var data = {userId: userId};

            if($scope.$state.includes('users')){
                data.forView=true;
            }else{
                data.forSaView=true;
            }

            ngSocket.emit('getUser', data);

            ngSocket.on('userSelected', function (data) {
                if (data.err) {
                    alert(data.message)
                }
                $scope.couter = data.count;
                $scope._createdUser = data.user;
                if($scope.$state.includes('superadmin-users')){
                    $scope._createdUser.implementing={
                        text: data.user.school.name,
                        value: data.user.school.id
                    }
                }else{
                    $scope._createdUser.roleId = +$scope._createdUser.role.id;
                    $scope._createdUser.classId = 'Выбрать класс';
                    if(data.user.learner!==null){
                        $scope._createdUser.classId = +data.user.learner.class.id;
                    }
                }
            });
        }
        $scope.switchingBetweenFunctionsRegUserAndSaveAndContinue = 0; // вызов/переключение между
        // функциями regUser - 0 и saveAndContinue - 1
        $scope.regUser = function () {
            if (userId) {
                $scope._createdUser.id = userId;
                console.log($scope._createdUser);
                ngSocket.emit('editUser', $scope._createdUser);
                return;
            }
            $scope.switchingBetweenFunctionsRegUserAndSaveAndContinue = 0;
            ngSocket.emit('createNewUser', $scope._createdUser);
        };

        //
        $scope.saveAndContinue = function () {
            $scope.switchingBetweenFunctionsRegUserAndSaveAndContinue = 1;
            ngSocket.emit('createNewUser', $scope._createdUser);
        };
        //
        ngSocket.on('addLearner', function (data) {
            if (data.err) {
                alert(data.message);
            }
        });

        ngSocket.on('userCreated', function (data) {
            if (data.err) {
                alert(data.message)
            } else {
                if ($scope.switchingBetweenFunctionsRegUserAndSaveAndContinue) {
                    $scope.showTheWindowOnSuccess = 1;
                    $scope._createdUser.init();
                    // ввести обнуление параметров ввода и добавить выскакивающую плашку
                    return false;
                }
                $scope.$state.go('^');
            }
            $stateParams.newUser = null;
        });

        ngSocket.on('userChanged', function (data) {
            if (data.err) {
                alert(data.message)
            } else {
                $scope.$state.go('^');
            }
            $stateParams.newUser = null;
        });

        $scope.pageTitle = "";
        $scope.pageBreadcrumbs = "";

        $scope.genPass = function () {
            var arr = [];
            var str = 'A0123456789KOALXZqwertyuiopa*%$-=+!@!@3@klzx/cvbnm';
            var rndnumber;

            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min)) + min;
            }

            for (var i = 0; i <= 20; i++) {
                rndnumber = (getRandomInt(0, str.length));
                arr += str[rndnumber];
            }
            $scope._createdUser.password = arr;
        };

    }]).controller('UsersView', ['$scope', 'ngSocket', '$stateParams', function ($scope, ngSocket, $stateParams) {
        //
        $scope.userId = $stateParams.id;
        $scope._userSelected ={};
        ngSocket.on('userSelected', function (data) {
            if (data.err) {
                alert(data.message);
            }
            $scope._userSelected = data.user;
        });
        //
        $scope.userId = $stateParams.id;
        $scope._userSelected ={};
        
        var data = {userId: $scope.userId};
        if($scope.$state.includes('users')){
            data.forView=true;
        }else{
            data.forSaView=true;
        }
        
        ngSocket.emit('getUser', data);
        ngSocket.on('userSelected', function (data) {
            if (data.err) {
                alert(data.message);
            }
            $scope._userSelected = data.user;
        });
    }]);
});