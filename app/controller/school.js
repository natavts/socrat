
'use strict';
const School = require('../models/').School;

exports.list = function (req,res) {
    School.findAll({
        where: {
            isArchive: false,
            name : {$iLike:`%${req.query.text}%`}
        }
    }).then(function (schools) {
        let data = [];
        schools.forEach(function (school) {
            data.push({value:school.id,text:school.name});
        });
        return res.json(data);
    },function (err) {
        return res.json({err});
    });
};