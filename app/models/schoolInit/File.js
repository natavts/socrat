/**
 * Created by piligrim on 28.06.16.
 */
'use strict';
const Sequelize = require('sequelize');

const attributes = {
    fileName: {
        type: Sequelize.STRING
    },
    sourceFileName: {
        type: Sequelize.STRING
    },
    uploadedBy: {
        type: Sequelize.STRING
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

module.exports.attributes = attributes;