/**
 * Created by piligrim on 6/13/16.
 */
'use strict';
const Sequelize = require('sequelize');

const attributes = {
    // time: {
    //     type: Sequelize.STRING,
    //     allowNull: false,
    //     unique: true
    // },
    message: {
        type: Sequelize.STRING
    }
};

module.exports.attributes = attributes;