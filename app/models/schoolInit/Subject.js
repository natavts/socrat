/**
 * Created by Alex on 06.06.2016.
 */
'use strict';
var Sequelize = require('sequelize');

var attributes = {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    description: {
        type: Sequelize.STRING
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

module.exports.attributes = attributes;