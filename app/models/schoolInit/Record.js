/**
 * Created by staloverov on 09.06.2016.
 */
'use strict';
var Sequelize = require('sequelize');

const attributes = {
    event_ts: {
        type : Sequelize.DATE
    },
    topic: {
        type : Sequelize.STRING
    },
    tag: {
        type:Sequelize.STRING
    },
    canDelete: {
        type: Sequelize.BOOLEAN
    },
    availableToAll : {
        type: Sequelize.BOOLEAN
    },
    isArchive: {
        type: Sequelize.BOOLEAN
    }
};

var options = {
    freezeTableName : true
    // setterMethods : {
    //     setTeacher : function(value) {
    //         this.setDataValue('teacherId', value.id);
    //     }
    // },
    // getterMethods : {
    //     setTeacher : function() {
    //         return this.getDataValue('teacherId');
    //     }
    // }
};

module.exports.attributes = attributes;
module.exports.options = options;
