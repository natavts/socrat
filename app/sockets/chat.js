/**
 * Created by piligrim on 6/13/16.
 */
'use strict';
const Chat= require('../models/').Chat;

module.exports = function(socket, data) {
    if(!(+data.scheduleId && data.message.trim().length)){
        return false;
    }
    var _messageData = {
        message: data.message,
        userId: socket.request.user.id,
        scheduleId: +data.scheduleId
    };

    Chat.create(_messageData)
        .then(function (message) {
            const resp = {
                err:0,
                message: message,
                user: {
                    firstName:socket.request.user.firstName,
                    lastName:socket.request.user.lastName,
                    patronymic:socket.request.user.patronymic
                }
            };
            socket.to('lesson:'+(+data.scheduleId)).emit('catchMessageRow', resp);
            //пока возвращаем то же, что и для всех остальных
            socket.emit('catchMessageRow', resp);
        }).catch(function (err) {
            socket.emit('catchMessageRow', {
                err:1,
                message: err
            });
        })
};