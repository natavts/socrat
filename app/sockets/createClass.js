/**
 * Created by rabota on 03.06.2016.
 */
'use strict';
const Class = require('../models/').Class;
module.exports = function (socket, data) {
    if ((!data.className)) {
        socket.emit('createClass',
            {err: 1, message: 'Incorrect name classes'}
        );
        return
    }

    const _classData = {
        className: data.className,
        commentaries: data.commentaries,
        isArchive: false,
        schoolId : +socket.request.user.schoolId
    };
    Class.create(_classData).then( function (classes) {
        socket.emit('createClass', {
            'err': 0,
            newClass: {
                id: classes.id,
                className: classes.className
            }
        })
    }).catch(function (err) {
        socket.emit('createClass',
            {err: 1, message: err.message}
        );
    });
};

    
