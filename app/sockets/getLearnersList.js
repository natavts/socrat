/**
 * Created by emmtech on 22.06.16.
 */
'use strict';

const Learner = require('../models/').Learner;
const User = require('../models').User;

module.exports = function (socket, data) {
    let where = {
        isArchive: false
    };
    let include = [{model:User,attributes: ['id','lastName', 'firstName', 'patronymic']}];
    if(data.classId !== undefined && data.classId) {
        where.classId = +data.classId;
    }
    if(data.scheduleId!== undefined && data.scheduleId) {
        where.scheduleId = +data.scheduleId;
    }
    Learner.findAll({
        include,
        where
    }).then(function (result) {
        socket.emit('learnerList', {
            'err': 0,
            data: result
        });
    }).catch(function (err) {
        socket.emit('learnerList',
            {err: 1, message: err.message}
        );
    });
};