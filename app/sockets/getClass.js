/**
 * Created by rabota on 04.06.2016.
 */
'use strict';
const Class = require('../models/').Class;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('catchClass',
            {err: 1, message: 'Undefined class identifier'}
        );
        return
    }

    Class.findById(+data.id)
        .then(function (result) {
            socket.emit('catchClass', {
                'err': 0,
                data: result
            });
        }).catch(function (err) {
        socket.emit('catchClass',
            {err: 1, message: err.message}
        );
    })
};