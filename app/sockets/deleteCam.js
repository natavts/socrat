/**
 * Created by piligrim on 20.06.16.
 */
'use strict';
const Camera = require('../models/').Camera;

module.exports = function (socket, data) {
    Camera.findById(+data.id).then(
        function (result)
        {
            result.officeId = null;

            return result.save().then(()=>{
                socket.emit('cameraDeleted', {
                    'err': 0
                });
            }).catch(function (err) {
                socket.emit('cameraDeleted',
                    {err: 1, message: err.message}
                );
            })
        })
};