/**
 * Created by emmtech on 09.06.16.
 */
'use strict';

const Subject = require('../models/').Subject;
const Class = require('../models/').Class;
const Lesson = require('../models/').Lesson;
module.exports = function (socket, data) {
    var _offset = data.offset ? parseInt(data.offset) : 0;
    var _limit = data.limit ? parseInt(data.limit) : 0;
    var _order = [];
    var where = {
        isArchive: false
    };
    
    if (data.sort != undefined) {
        for (var key in data.sort) {
            if (key !== null && data.sort[key]) {
                _order.push([key, (data.sort[key] - 1 ? 'DESC' : 'ASC')])
            }
        }
    }
    Subject.findAndCountAll({
        where: where,
        offset: _offset,
        limit: _limit,
        order: _order,
        include: [Class, Lesson]
    }).then(function (result) {
        socket.emit('subjectList', {
            'err': 0,
            data: result
        });
    }).catch(function (err) {
        socket.emit('subjectList',
            {err: 1, message: err.message}
        );
    });
};