/**
 * Created by piligrim on 6/13/16.
 */
'use strict';
module.exports = function(socket, data) {
    if(!(+data.scheduleId && data.name.trim().length)){
        return false;
    }
    socket.to('lesson:'+(+data.scheduleId)).emit('restartSteamFromTeacherCam', data);
};