 /**
 * Created by Роман on 22.04.2015.
 */
var comments = require('../models/commentsModel');
var	conference = require('../models/conferencemodel');
module.exports = function(socket,data){
    conference.checkPass(data.sn, data.password,function(d){
        if(!d.err&&d.body!=null){
            var user=d.body.members.filter(function(member){
                return member.user_id.toString()===socket.request.user.id;
            }).pop();
            socket.emit('open',{err:d.err, comments: d.body.comments, body:d.body, user:user});
        }else{
            conference.getConferenceOne(socket.request.user,{sn:data.sn},function(doc){
                if(doc.err||doc.body==null){
                    socket.emit('open',{err:doc.err});
                }else{
                    var user=doc.body.members.filter(function(member){
                        return member.user_id.toString()===socket.request.user.id;
                    }).pop();	
                    socket.emit('open',{err:0, comments: doc.body.comments, body:doc.body, user:user});
                }
            });
        }
    });
}