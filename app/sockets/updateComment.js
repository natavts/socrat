/**
 * Created by Роман on 28.05.2015.
 */
var comments = require('../models/commentsModel');
module.exports = function(socket,data){
    var User=socket.request.user;
    comments.comUpdate({sn:data.sn,from:User.id,id:data.hash,text:data.text},function(err,doc){
        if(err){
            return socket.emit('updateComment',{hash:data.hash, err:err});
        }else {
            console.log(doc);
            if (doc.for.length) {
                doc.for.forEach(function (member) {
                    socket.passport.filterSocketsByUser(socket.io, function (u) {
                        if (u.id != undefined && u.id) {
                            return u.id === member.real_user.toString();
                        }
                        return false;
                    }).forEach(function (s) {
                        s.emit('updateComment', {
                            sn: data.sn,
                            hash: data.hash,
                            err: 0,
                            body: {
                                confId: doc.confId,
                                from: from,
                                update_date: doc.update_date,
                                status: doc.status,
                                text: doc.text,
                                _id: doc._id,
                                old: doc.old
                            }
                        });
                    });

                });
            }
            return socket.emit('updateComment', {err: 0, body: doc, hash: data.hash, sn: data.sn});
        }
    });
};