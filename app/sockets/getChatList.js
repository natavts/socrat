/**
 * Created by emmtech on 24.06.16.
 */
'use strict';
const Chat = require('../models/').Chat;
const User = require('../models/').User;
module.exports = function (socket, data) {
    let offset = data.offset ? parseInt(data.offset) : 0;
    let limit = data.limit ? parseInt(data.limit) : null;
    let order = [['id', 'DESC']];
    let include = [{model:User,attributes: ['lastName', 'firstName', 'patronymic']}];
    let where = {
        scheduleId: data.scheduleId
    };
    Chat.findAll({
        include,
        order,
        limit,
        offset,
        where
    }).then(function (result) {
        socket.emit('chatList', {
            'err': 0,
            data: result.sort(function (a, b) {
                if (a.id > b.id) {
                    return 1;
                }
                if (a.id < b.id) {
                    return -1;
                }
                return 0;
            })
        });
    }).catch(function (err) {
        socket.emit('chatList',
            {err: 1, message: err.message}
        );
    });
};