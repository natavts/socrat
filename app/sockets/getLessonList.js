/**
 * Created by piligrim on 01.06.16.
 */
'use strict';

var Lesson = require('../models/').Lesson;

module.exports = function (socket, data) {
    var _offset = data.offset ? parseInt(data.offset) : 0;
    var _limit = data.limit ? parseInt(data.limit) : null;
    let _order = [];
    var where = {
        isArchive: false,
        schoolId : +socket.request.user.schoolId
    };

    //функция сортировки по возрастанию\убыванию
    if (data.sort != undefined) {
        for (var key in data.sort) {
            if (key !== null && data.sort[key]) {
                _order.push([key, (data.sort[key] - 1 ? 'DESC' : 'ASC')])
            }
        }
    }

    if (data.filter !== undefined) {
        if (data.filter.name !== undefined && data.filter.name.trim()) {
            where.lessonName = {
                $iLike: `%${data.filter.name}%`
            };
        }

        if (data.filter.description !== undefined && data.filter.description.trim()) {
            where.description = {
                $iLike: `%${data.filter.description}%`
            };
        }
    }

    Lesson.findAndCountAll({
        where: where,
        offset: _offset,
        limit: _limit,
        order: _order
    }).then(function (result) {
        socket.emit('lessonsList', {
            'err': 0,
            lessons: result
        });
    }).catch(function (err) {
        socket.emit('lessonsList',
            {err: 1, message: err.message}
        );
    });
};