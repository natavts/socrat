/**
 * Created by Alex on 01.06.2016.
 */
'use strict';
var User = require('../models/').User;
const Learner = require('../models/').Learner;
const Role = require('../models/').Role;
const Class = require('../models/').Class;
const School = require('../models/').School;
module.exports = function(socket, data) {
    if (!data.userId) {
        socket.emit('userSelected',
            {err: 1, message: 'Undefined user identifier'}
        );
        return
    }
    let include = [];
    if (data.forView!==undefined && data.forView){
        include.push({
            model: Learner,
            include: [{
                model:Class,
                attributes:['id','className']
            }],
            attributes:['classId']
        },{
            model:Role,
            attributes:['id','description']
        })
    } else if (data.forSaView!==undefined && data.forSaView){
        include.push({
            model: School,
            attributes:['id','name','numberOfClasses']
        })
    } else {
        include.push(Learner);
    }

    User.findById(data.userId, {
        attributes:['email','firstName','lastName','patronymic','phone','introduce', 'id', 'username'],
        include
    })
        .then(function(user) {
            socket.emit('userSelected', {
                'err': 0,
                user: user
            });
        }).catch(function (err) {
        socket.emit('userSelected',
            {err: 1, message: err.message}
        );
    })
};
