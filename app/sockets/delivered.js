
var comments = require('../models/commentsModel');
module.exports = function(socket,data){
    var User=socket.request.user;
    var status=2;
    comments.updateStatus({status:status,from:data.body.from.user_id,id:data.body._id,user_id:User._id},function(err,doc){
        socket.passport.filterSocketsByUser(socket.io, function (u) {
            if (u.id != undefined && u.id) {
                return u.id === doc.from.real_user.toString();
            }
            return false;
        }).forEach(function (s) {
            s.emit('commentStatus', {
                sn: data.sn,
                hash: data.hash,
                err: 0,
                body: {
                    status: doc.status,
                    text: doc.text,
                    _id: doc._id,
                }
            });
        });
    })
};