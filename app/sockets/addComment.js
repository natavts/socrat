var comments = require('../models/commentsModel');
var	conference = require('../models/conferencemodel');
module.exports = function(socket,data){
    var User=socket.request.user;
    conference.getUsersAndComments({sn:data.sn,for:data.for,from:User.id},function(err,doc){
		if(err){
			return socket.emit('comment',{hash:data.hash, err:1});
		}else{
			var from=null;
			doc.users.forEach(function(user){
				if(user.user_id==User.id){
				    from = {user_id:user.id,user_name:user.user_name,real_user:User.id};
                    comment= new comments({
                        confId:doc.id,
                        from:from,
                        text:data.text,
                        for:data.for,
                        update_date:new Date(),
                        status:1
                    });
                    comment.save(function(err,comm){
                        if(err){
                            socket.emit("commentStatus",{hash:data.hash, err:1})
                        }else{
                            doc.comments.push(comm.id);
											  if(doc.comments.length>200){
													doc.comments.shift();
                            }
                            doc.save(function(err){
                                if(err){
                                    socket.emit("commentStatus",{hash:data.hash, err:1, id:comm.id})
                                }else{
                                    socket.emit("commentStatus",{hash:data.hash, err:0, id:comm.id, status:1})
                                }
                            })
                            delete from.real_user;
                            socket.passport.filterSocketsByUser(socket.io, function(u){
                                if(u.id!=undefined && u.id){
                                    return u.id === user.user_id.toString();
                                }
                                return false;
                            }).forEach(function(s){
                                s.emit('comment', {
                                    sn:data.sn,
                                    hash:data.hash,
                                    err:0,
                                    body: {
                                        confId:doc.id,
                                        from:from,
                                        text:data.text,
                                        _id:comm.id,
                                        status:comm.status
                                    }
                                });
                            });
                        }
                    });
				}
			})
		}
    });
}