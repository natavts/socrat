/**
 * Created by emmtech on 09.07.16.
 */
 'use strict';

const Record = require('../models/').Record;
const Video = require('../models/').Video;
const Class = require('../models/').Class;
const Lesson = require('../models/').Lesson;
const User = require('../models/').User;
module.exports = function (socket, data) {
    let include = [];
    if(!data.id) {
        socket.emit('catchRecord', {
            err: 1,
            message: 'Undefined record identifier'
        });
        return false;
    }
    if(socket.request.user.roleId === 2){
        socket.emit('editVideo', {});
    }
    
    include.push({model: Class, attributes:['className']},
    {model: Video},
    {model: Lesson, attributes:['lessonName']},
    {model: User, attributes:['firstName', 'lastName', 'patronymic']});
    
    Record.findById(+data.id, {
        include
    }).then((result)=> {
        socket.emit('catchRecord', {
            err: 0,
            data: result
        });
    }).catch((err)=>{
        socket.emit('catchRecord', {
            err: 1,
            message: err.message
        })

    })
};