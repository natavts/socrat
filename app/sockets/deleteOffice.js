/**
 * Created by piligrim on 6/16/16.
 */
'use strict';
const Office = require('../models/').Offices;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('officeDeleted',
            {err: 1, message: 'Undefined office identifier'}
        );
        return
    }
    Office.findById(+data.id).then(
        function (result)
        {
            result.isArchive = true;

            return result.save().then(function(result){
                socket.emit('officeDeleted', {
                    'err': 0,
                    class: {
                        officeId : result.id,
                        officeName: result.number
                    }
                });
            }).catch(function (err) {
                socket.emit('officeDeleted',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('officeDeleted',
            {err: 1, message: err.message}
        );
    })
};
