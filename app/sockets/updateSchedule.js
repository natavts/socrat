/**
 * Created by emmtech on 16.06.16.
 */
'use strict';
const Schedule = require('../models/').Schedule;
const Record = require('../models/').Record;
const Lesson = require('../models/').Lesson;
const Class = require('../models/').Class;
const Office = require('../models/').Offices;
const Camera = require('../models/').Camera;
const Video = require('../models/').Video;
const child_process = require('child_process');
const psTree = require('ps-tree');

module.exports = function (socket, data) {
    // console.log('step 1');
    let where = {};
    if (!data.id) {
        socket.emit('scheduleUpdate',
            {err: 1, message: 'Undefined schedule identifier'}
        );
        return
    }
    Schedule.findById(+data.id, {
        include: [Lesson, Class]
    }).then(function (result) {
        if(data.officeId) {
            where = {
                id: {$ne: +data.id},
                officeId: +data.officeId,
                state: {$or:[1, 2]}
            }
        } else {
            where = {createdAt: null};
        }
        return Schedule.count({
            where
        }).then(function (dataCount) {
            // console.log('step 2');
            // console.log(dataCount);
             if (!dataCount) {
                 socket.emit('stopFuckingRoom', {err: 0});
                var changes = 0;
                var webCam = null;
                if (data.duration !== undefined && +data.duration > 0 && +result.duration !== +data.duration) {
                    result.duration = +data.duration;
                    changes = 1
                }
                if (data.topic !== undefined && data.topic !== null && result.topic !== data.topic) {
                    result.topic = data.topic;
                    changes = 1
                }
                data.changeState = false;
                if (data.state !== undefined && data.state !== null && +result.state !== +data.state) {
                    data.changeState = true;
                    result.state = +data.state;
                    changes = 1
                }
                if (data.officeId !== undefined && +data.officeId) {
                    result.officeId = +data.officeId;
                    webCam = `${data.officeId}D${Date.now()}`;
                    changes = 2
                }
                if(data.tag !== undefined && data.tag) {
                    result.tag = data.tag;
                }
                if(data.durationEnd !== undefined && data.durationEnd) {
                    result.durationEnd = data.durationEnd;
                }
                if (changes) {
                    return result.save().then(()=> {
                        // console.log(result.officeId);
                        if (changes - 1) {
                            return Office.update({webCam}, {
                                where: {
                                    $and: [
                                        ['"webCam" IS NOT NULL'],
                                        {id: result.officeId}
                                    ]
                                }
                            })
                        } else {
                            return new Promise((resolve, reject)=>resolve());
                        }
                    }).then(()=> {
                        // console.log('step 3');
                        return successStartLesson(result, data)
                    })
                } else {
                    return socket.emit('scheduleUpdate', {
                        'err': 0,
                        data: result
                    });
                }
            } else {
                 socket.emit('stopFuckingRoom', {err: 1});
             }
        })
    })
        .catch(function (err) {
            socket.emit('scheduleUpdate',
                {err: 1, message: err.message}
            );
        });
    //Ответ и запись видео
    function successStartLesson(result, data) {
        socket.emit('scheduleUpdate', {
            'err': 0,
            data: result
        });
        if (!data.changeState) {
            return false;
        }
        //Опасная остановка процесса записи видео
        //ещё и нерабочая(( ломает файл
        var kill = function (pid, signal, callback) {
            signal = signal || 'SIGKILL';
            callback = callback || function () {
                };
            var killTree = true;
            if (killTree) {
                psTree(pid, function (err, children) {
                    [pid].concat(
                        children.map(function (p) {
                            return p.PID;
                        })
                    ).forEach(function (tpid) {
                        try {
                            process.kill(tpid, signal)
                        }
                        catch (ex) {
                        }
                    });
                    callback();
                });
            } else {
                try {
                    process.kill(pid, signal)
                }
                catch (ex) {
                }
                callback();
            }
        };
        //если статус изменился

        return Camera.findAll({
            where: {
                officeId: +result.officeId
            }
        }).then(function (cameras) {
            let camerasExec = [];
            // console.log('step 4');
            cameras.forEach(function (camera) {
                //остановить запись видео с этих камер
                if (+camera.recPID) {
                    kill(+camera.recPID);
                    camera.recPID = 0;
                }
                //начать новую запись с камер, если новый статус урока "Начат"
                if (+result.state === 1) {
                    const name = `${+result.officeId}File${Date.now()}`;
                    // console.log(camera.url, name);
                    let rec = child_process.spawn('ffmpeg', ['-i', camera.url, '-vcodec', 'libx264', `public/video/${name}.ts`]);
                    camera.recPID = rec.pid;
                    // console.log('step 5');
                    camerasExec.push(Video.create({
                        isArchive: false,
                        scheduleId: +result.id,
                        cameraId: +camera.id,
                        ts: true,
                        name
                    }));
                    //Отслеживаем событие завершения записи;
                    rec.stderr.addListener('exit', function () {
                        Camera.findOne({
                            where: {
                                recPID: +rec.pid
                            }
                        }).then((resCam)=> {
                            resCam.recPID = 0;
                            return resCam.save();
                        }).then((resCam)=> {
                            Video.update({
                                isArchive: true
                            }, {
                                where: {
                                    isArchive: false,
                                    officeId: +resCam.officeId
                                }
                            })
                        });
                    });
                } else {

                }
                camerasExec.push(camera.save());
            });

            if (camerasExec.length) {
                return Promise.all(camerasExec);
            } else {
                return new Promise((resolve, reject)=>resolve());
            }

        }).then(()=> {
            if (+result.state === 3) {
                return Video.findAll({
                    where: {
                        scheduleId: +result.id,
                        ts: true
                    },
                    order: [['id', 'ASC']]
                }).then((videos)=> {
                    let promiseArr = [];
                    let videoStrArr = {};
                    /**
                     * перебераем массив видео текущего урока
                     * собираем строку из имён видео склееных `|` для каждой камеры
                     */

                    videos.forEach(function (video) {
                        videoStrArr[video.cameraId] = videoStrArr[video.cameraId] ? videoStrArr[video.cameraId] + '|' + `public/video/${video.name}.ts` : `public/video/${video.name}.ts`
                    });
                    /**
                     * если длинна массива больше 1, то склеиваем куски видео
                     * при помощи собранной строки
                     */
                    let videoCam;
                    for (videoCam in videoStrArr) {
                        const name = `${+result.officeId}File${Date.now()}`;
                        child_process.exec(`ffmpeg -i "concat:${videoStrArr[videoCam]}" -c copy -bsf:a aac_adtstoasc public/video/${name}.mp4`);
                        promiseArr.push(
                            Video.create({
                                cameraId: +videoCam,
                                scheduleId: +result.id,
                                name,
                                ts: false,
                                isArchive: false
                            })
                        );
                    }
                    let record = {
                        event_ts: result.begin_ts,
                        lessonId: +result.lessonId,
                        classId: +result.classId,
                        userId: +result.userId,
                        topic: result.topic,
                        scheduleId: result.id,
                        isArchive: false,
                        tag: result.tag
                    };
                    return Promise.all(promiseArr).then(()=> {
                        Record.create(record).then((resultRec)=> {
                            return Video.update({
                                scheduleId: null,
                                recordId: resultRec.id
                            }, {
                                where: {
                                    scheduleId: +result.id,
                                    ts: false
                                }
                            }).then(()=> {
                                return Video.destroy({
                                    where: {
                                        scheduleId: +result.id,
                                        ts: true
                                    }
                                })
                            });
                        })
                    });

                });

            }
        })
    }
};
