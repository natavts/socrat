/**Created by piligrim on 01.06.16.**/
var Lesson= require('../models/').Lesson;
module.exports = function(socket, data) {
    if (!(data.lessonName)) {
        socket.emit('lessonCreated',
            {err: 1, message: 'Incorrect lesson name'}
        );
        return
    }

    var _lessonData = {
        lessonName : data.lessonName,
        description : data.description,
        isArchive : false,
        schoolId : +socket.request.user.schoolId
    };

    Lesson.create(_lessonData)
        .then(function (lesson) {
            socket.emit('lessonCreated', {
                'err': 0,
                newLesson: {
                    lessonId : lesson.id,
                    lessonName: lesson.lessonName
                    //schoolId : +socket.request.user.schoolId
                }
            });
        }).catch(function (err) {
        socket.emit('lessonCreated',
            {err: 1, message: err.message}
        );
    })
};