/**
 * Created by piligrim on 02.06.16.
 */
var Lesson = require('../models/').Lesson;

module.exports = function(socket, data) {
    if (!data.lessonId) {
        socket.emit('catchLesson',
            {err: 1, message: 'Undefined lesson identifier'}
        );
        return
    }
    
    Lesson.findById(+data.lessonId)
        .then(function (result) {
            socket.emit('catchLesson', {
                'err': 0,
                lesson: result
            });
        }).catch(function (err) {
        socket.emit('catchLesson',
            {err: 1, message: err.message}
        );
    })
};