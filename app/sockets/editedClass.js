/**
 * Created by rabota on 04.06.2016.
 */
'use strict';
const Class = require('../models/').Class;

module.exports = function (socket, data) {
    if (!data.id) {
        socket.emit('classEdited',
            {err: 1, message: 'Undefined class identifier'}
        );
        return
    }
    Class.findById(+data.id).then(
        function (result) {
            result.className = data.className;
            result.commentaries = data.commentaries;
2
            result.save().then(function (result) {
                socket.emit('classEdited', {
                    'err': 0,
                    classes: {
                        classId: result.id,
                        className: result.className
                    }
                });
            }).catch(function (err) {
                socket.emit('classEdited',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('classEdited',
            {err: 1, message: err.message}
        );
    })
};