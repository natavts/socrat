/**
 * Created by emmtech on 05.07.16.
 */
'use strict';

const Record = require('../models/').Record;
const User = require('../models/').User;
const Lesson = require('../models/').Lesson;
const Class = require('../models/').Class;
module.exports = function (socket, data) {
    const _offset = data.offset ? parseInt(data.offset) : 0;
    const _limit = data.limit ? parseInt(data.limit) : null;
    let _order = [];
    let include = [];
    include.push({model: Lesson, attributes: ['lessonName']},
    {model: Class, attributes:['className']},
    {model: User, attributes:['firstName', 'lastName', 'patronymic']});
    let where = {
        isArchive: false
    };

    if(data.filter !== undefined) {

            if (data.filter.lessonId !== undefined && +data.filter.lessonId) {
                where.lessonId = +data.filter.lessonId
            }
            if (data.filter.classId !== undefined && +data.filter.classId) {
                where.classId = +data.filter.classId
            }

        if(data.filter.tag !== undefined && data.filter.tag.trim()) {
            where.tag = {
                $iLike: `%${data.filter.tag.trim()}%`
            }
        }
        if(data.filter.topic !== undefined && data.filter.topic.trim()) {
            where.topic = {
                $iLike: `%${data.filter.topic.trim()}%`
            }
        }
    }

    Record.findAndCountAll({
        where: where,
        offset: _offset,
        limit: _limit,
        order: _order,
        include
    }).then(function (result) {
        socket.emit('recordList', {
            'err': 0,
            data: result
        });
    }).catch(function (err) {
        socket.emit('recordList', {
            err: 1, 
            message: err.message
        });
    });
};