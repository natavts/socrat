/**
 * Created by Alex on 01.06.2016.
 */
'use strict';
const User = require('../models/').User;
const Role = require('../models/').Role;
const Class = require('../models/').Class;
const Learner = require('../models/').Learner;
const School = require('../models/').School;
module.exports = function (socket, data) {
    const role=socket.request.user.roleId;
    const offset = data.offset ? parseInt(data.offset) : 0;
    const limit = data.limit ? parseInt(data.limit) : null;
    let _order = [];
    let include = [];

    if(data.sort!==undefined) {
        for (var key in data.sort) {
            if(key!==null && data.sort[key] && key!=='fio') {
                _order.push([key, data.sort[key]-1 ? 'DESC':'ASC'])
            }
        }
        if(data.sort['fio']!==undefined && data.sort['fio']){
            _order.push(['lastName', data.sort['fio']-1 ? 'DESC':'ASC']);
            _order.push(['firstName', data.sort['fio']-1 ? 'DESC':'ASC']);
            _order.push(['patronymic', data.sort['fio']-1 ? 'DESC':'ASC']);
            data.sort['fio'] = 0;
        }
    }

    let where = {
        isArchive: false
    };
    
    if(data!=undefined){
        
        if(data.filter!==undefined){
            if(data.filter.introduce!==undefined && data.filter.introduce.trim()){
                where.introduce = {
                    $iLike : `%${data.filter.introduce.trim()}%`
                };
            }
            if(data.filter.name!==undefined && data.filter.name.trim()){
                where.$or = [
                    {firstName :{
                        $iLike : `%${data.filter.name.trim()}%`
                    }},
                    {lastName :{
                        $iLike : `%${data.filter.name.trim()}%`
                    }},
                    {patronymic :{
                        $iLike : `%${data.filter.name.trim()}%`
                    }}
                ]
            }
            if(data.filter.firstName!==undefined && data.filter.firstName.trim()) {
                where.firstName = {
                        $iLike : `%${data.filter.firstName.trim()}%`
                };
            }
            if(data.filter.lastName!==undefined && data.filter.lastName.trim()) {
                where.lastName= {
                        $iLike : `%${data.filter.lastName.trim()}%`
                };
            }
            if(data.filter.patronymic!==undefined && data.patronymic.trim()) {
                where.patronymic = {
                        $iLike : `%${data.filter.patronymic.trim()}%`
                }
            }
            if(data.filter.role!==undefined && data.filter.role.trim()) {
                include.push({
                    model: Role,
                    where: {description: {$iLike: `%${data.filter.role.trim()}%`}},
                    attributes:['description']
                });
            }else{
                include.push({
                    model:Role,
                    attributes:['description']
                })
            }
            if(data.filter.class!==undefined && data.filter.class.trim()) {
                include.push({
                    model: Learner,
                    include: [{
                        model:Class,
                        attributes:['className'],
                        where: {className: {$iLike: `%${data.filter.class.trim()}%`}}
                    }],
                    attributes:['classId']

                });
            } else if (data.listView!==undefined && data.listView) {
                include.push({
                    model: Learner,
                    include: [{
                        model:Class,
                        attributes:['className']
                    }],
                    attributes:['classId']
                })
            }

            if(data.filter.email!==undefined && data.filter.email.trim()){
                where.email = {
                    $iLike: `%${data.filter.email.trim()}%`
                };
            }

            if(data.filter.phone!==undefined && data.filter.phone.trim()){
                where.description = {
                    $iLike: `%${data.filter.phone.trim()}%`
                };
            }

            if(data.filter.implementing!==undefined && data.filter.implementing.trim()){
                where.description = {
                    $iLike: `%${data.filter.implementing.trim()}%`
                };
            }
        }
    }

    // Todo : право на просмотр списка пользователей
    // if(!role||role>2){
    //     socket.emit('userListSelected', {
    //         'err': 1,
    //         message: 'no access'
    //     });
    //     return false;
    // }

    if(data.teacherList && role === 2) {
        where.roleId = 3;
        where.schoolId = +socket.request.user.schoolId;
    } else if(+role===1){
            where.roleId = 2;
        } else {
            where.roleId = {$gt:2};
            where.schoolId = +socket.request.user.schoolId;
        }

    
    if (data.listSa!==undefined && data.listSa) {
        include.push({
            model: School,
            attributes:['numberOfClasses', 'name']
        })
    }

    User.findAndCountAll({
        where,
        offset,
        limit,
        order: _order,
        include
    }).then(function (result) {
            socket.emit('userListSelected', {
                'err': 0,
                users: result
            });
        }).catch(function (err) {
        socket.emit('userListSelected',
            {err: 1, message: err.message}
        );
    })
};