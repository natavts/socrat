/**
 * Created by emmtech on 17.06.16.
 */
'use strict';

const Schedule = require('../models/').Schedule;
module.exports = function (socket, data) {
    if(!data.id){
        socket.emit('deleteSchedule', {
            err: 1,
            message: 'Undefined schedule identifier'
        });
        return
    }
    Schedule.findById(+data.id).then(
        function (result)
        {
            result.isArchive = true;

            return result.save().then(function(){
                socket.emit('deleteSchedule', {
                    'err': 0
                });
            }).catch(function (err) {
                socket.emit('deleteSchedule',
                    {err: 1, message: err.message}
                );
            })
        }).catch(function (err) {
        socket.emit('deleteSchedule',
            {err: 1, message: err.message}
        );
    })
};