/**
 * Created by piligrim on 6/16/16.
 */
'use strict';
const Office = require('../models/').Offices;

module.exports = function(socket, data) {
    if (!data.id) {
        socket.emit('catchOffice',
            {err: 1, message: 'Undefined office identifier'}
        );
        return
    }

    Office.findById(+data.id)
        .then(function (result) {
            socket.emit('catchOffice', {
                'err': 0,
                data: result
            });
        }).catch(function (err) {
        socket.emit('catchOffice',
            {err: 1, message: err.message}
        );
    })
};