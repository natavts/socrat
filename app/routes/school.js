/**
 * Created by piligrim on 22.06.16.
 */
var express = require('express');
var router = express.Router();
var school = require('../controller/school');
var passport = require('passport');

router.get('/', school.list);

module.exports = router;